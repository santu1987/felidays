-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 25, 2020 at 09:19 AM
-- Server version: 5.5.50-0+deb8u1
-- PHP Version: 5.6.36-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `felidays`
--

-- --------------------------------------------------------

--
-- Table structure for table `carritos`
--

CREATE TABLE IF NOT EXISTS `carritos` (
`idCarrito` int(11) NOT NULL,
  `idPaquete` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `detalle` text NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(10) NOT NULL,
  `tipoPago` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ciudades`
--

CREATE TABLE IF NOT EXISTS `ciudades` (
`idCiudad` int(11) NOT NULL,
  `idPais` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL,
  `imagen` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ciudades`
--

INSERT INTO `ciudades` (`idCiudad`, `idPais`, `nombre`, `estatus`, `imagen`) VALUES
(1, 112, 'Roma', 1, 'roma.jpg'),
(2, 4, 'Munich', 1, 'munich.jpg'),
(3, 82, 'Paris', 1, 'paris.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `contactos`
--

CREATE TABLE IF NOT EXISTS `contactos` (
`idContacto` int(11) NOT NULL,
  `idIdioma` int(11) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `direccion` text,
  `latitud` varchar(20) DEFAULT NULL,
  `longitud` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactos`
--

INSERT INTO `contactos` (`idContacto`, `idIdioma`, `email`, `telefono`, `direccion`, `latitud`, `longitud`) VALUES
(1, 1, 'info@felidays.com', '000000000', 'sin direccion por el momento', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
`idEmpresa` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `titulo` varchar(60) NOT NULL,
  `qSomos` text NOT NULL,
  `mision` text,
  `vision` text,
  `historia` text,
  `principio` text
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`idEmpresa`, `idIdioma`, `estatus`, `titulo`, `qSomos`, `mision`, `vision`, `historia`, `principio`) VALUES
(1, 1, 1, 'Quienes Somos*Misión*Visión*Historia*Principios', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.'),
(2, 2, 1, 'About Us*Mission*Vision*History*Principles', 'Inlges Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam lacinia lectus metus, sit amet molestie justo facilisis eu. Proin lacinia at lorem vel accumsan. Quisque hendrerit finibus nisi. Curabitur aliquam mi orci. Ut at eros aliquam, malesuada magna a, imperdiet libero. Etiam euismod, neque eget elementum semper, tortor odio ultricies ante, vitae porta orci massa blandit lorem. Sed eu ante porttitor, varius nunc vitae, facilisis purus.');

-- --------------------------------------------------------

--
-- Table structure for table `encuestas`
--

CREATE TABLE IF NOT EXISTS `encuestas` (
`idEncuesta` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `descripcion` text,
  `tipo` varchar(20) NOT NULL,
  `idtipo` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `nacionalidad` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `equipo`
--

CREATE TABLE IF NOT EXISTS `equipo` (
`idEquipo` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `funcion` varchar(60) NOT NULL,
  `descripcion` text NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equipo`
--

INSERT INTO `equipo` (`idEquipo`, `idIdioma`, `nombre`, `funcion`, `descripcion`, `foto`) VALUES
(1, 1, 'Humberto Parisi', 'CEO & Fundador de SoftParDay', 'Humberto es informatico graduado en el IUJO, desde joven ha practicado el desarrollo de software entre otras actividades referentes a la informatica', 'guia1.jpg'),
(2, 1, 'Daysi Rodriguez', 'CEO & Fundadora de Felidays', 'Daysi es abogada, especializada en derecho laborarl, pero ademas le encanta viajar y conocer nuevos destinos, lo cual hizo que tuviese una vision de crear un portal dedicado al turismo', 'guia2.jpg'),
(3, 1, 'Astrid Feliciani', 'CEO & Fundadora de Felidays', 'Astrid es abogada, especializada en derecho laborarl, pero ademas le encanta viajar y conocer nuevos destinos, lo cual hizo que tuviese una vision de crear un portal dedicado al turismo', 'guia2.jpg'),
(4, 2, 'Humberto Parisi', 'CEO & Founder of SoftParDay', 'Humberto is a computer graduate in the IUJO, since young has practiced software development among other activities related to computer science', 'guia1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `evaluaciones`
--

CREATE TABLE IF NOT EXISTS `evaluaciones` (
`idEvaluacion` int(11) NOT NULL,
  `idGuia` int(11) NOT NULL,
  `valor` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluaciones`
--

INSERT INTO `evaluaciones` (`idEvaluacion`, `idGuia`, `valor`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 2, 2),
(4, 2, 2),
(5, 3, 5),
(6, 3, 2),
(7, 4, 3),
(8, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE IF NOT EXISTS `footer` (
`idFooter` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `info` text NOT NULL,
  `columnas` text NOT NULL,
  `formasPagos` text,
  `redesSociales` text NOT NULL,
  `copy` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `formularios`
--

CREATE TABLE IF NOT EXISTS `formularios` (
`idFormulario` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `idPais` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `documentoIdentidad` varchar(20) DEFAULT NULL,
  `direccion` text,
  `ciudad` varchar(50) NOT NULL,
  `mensaje` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `formularios`
--

INSERT INTO `formularios` (`idFormulario`, `idIdioma`, `idPais`, `nombre`, `email`, `telefono`, `tipo`, `documentoIdentidad`, `direccion`, `ciudad`, `mensaje`) VALUES
(1, 1, 112, 'Humberto Parisi', 'prueba@prueba.com', '00000', 'contacto', '000000', 'direccion', 'Palermo', 'esto es un mensaje de prueba de registro'),
(2, 1, 112, 'Humberto Parisi', 'prueba@prueba.com', '00000', 'contacto', '000000', 'direccion', 'Palermo', 'esto es un mensaje de prueba de registro'),
(3, 1, 112, 'Humberto Parisi', 'prueba@prueba.com', '00000', 'contacto', '000000', 'direccion', 'Palermo', 'esto es un mensaje de prueba de registro');

-- --------------------------------------------------------

--
-- Table structure for table `guias`
--

CREATE TABLE IF NOT EXISTS `guias` (
`idGuia` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `idCiudad` int(11) NOT NULL,
  `imagenFondo` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guias`
--

INSERT INTO `guias` (`idGuia`, `idIdioma`, `nombre`, `estatus`, `imagen`, `descripcion`, `idCiudad`, `imagenFondo`) VALUES
(1, 1, 'Humberto Parisi', 1, 'guia1.jpg', 'Una pequena descripcion del guia. orem ipsum dolor sit amet, consectetur adipiscing elit. Integer non mi euismod, venenatis justo id, imperdiet velit. Etiam eget nisi vel nisl placerat consectetur. Maecenas efficitur neque in nisi auctor efficitur. Aenean placerat diam aliquet mi rhoncus pharetra. Integer maximus sapien eget felis ultricies cursus. In ut mattis diam. Suspendisse sodales, massa a hendrerit sollicitudin, diam nibh volutpat leo, in semper ante magna a mauris. Aliquam id leo aliquet, elementum mi sed, consequat urna. Sed auctor, velit id consectetur tincidunt, sapien mi mattis lacus, quis placerat ligula nibh eget nunc. Aliquam erat volutpat. Sed interdum metus sed neque egestas, vel porta orci porttitor. Suspendisse fringilla nisl risus, id pellentesque purus pretium at.\r\n\r\n ', 3, 'paris.jpg'),
(2, 2, 'Humberto Parisi', 1, 'guia1.jpg', 'A bit description of the guide. orem ipsum dolor sit amet, consectetur adipiscing elit. Integer non mi euismod, venenatis justo id, imperdiet velit. Etiam eget nisi vel nisl placerat consectetur. Maecenas efficitur neque in nisi auctor efficitur. Aenean placerat diam aliquet mi rhoncus pharetra. Integer maximus sapien eget felis ultricies cursus. In ut mattis diam. Suspendisse sodales, massa a hendrerit sollicitudin, diam nibh volutpat leo, in semper ante magna a mauris. Aliquam id leo aliquet, elementum mi sed, consequat urna. Sed auctor, velit id consectetur tincidunt, sapien mi mattis lacus, quis placerat ligula nibh eget nunc. Aliquam erat volutpat. Sed interdum metus sed neque egestas, vel porta orci porttitor. Suspendisse fringilla nisl risus, id pellentesque purus pretium at.\r\n\r\n', 3, 'paris.jpg'),
(3, 1, 'Rosa Perez', 1, 'guia2.jpg', 'Especialista en eventos parisinos. orem ipsum dolor sit amet, consectetur adipiscing elit. Integer non mi euismod, venenatis justo id, imperdiet velit. Etiam eget nisi vel nisl placerat consectetur. Maecenas efficitur neque in nisi auctor efficitur. Aenean placerat diam aliquet mi rhoncus pharetra. Integer maximus sapien eget felis ultricies cursus. In ut mattis diam. Suspendisse sodales, massa a hendrerit sollicitudin, diam nibh volutpat leo, in semper ante magna a mauris. Aliquam id leo aliquet, elementum mi sed, consequat urna. Sed auctor, velit id consectetur tincidunt, sapien mi mattis lacus, quis placerat ligula nibh eget nunc. Aliquam erat volutpat. Sed interdum metus sed neque egestas, vel porta orci porttitor. Suspendisse fringilla nisl risus, id pellentesque purus pretium at.', 3, 'paris.jpg'),
(4, 2, 'Rosa Perez', 1, 'guia2.jpg', 'Especialist in french events. orem ipsum dolor sit amet, consectetur adipiscing elit. Integer non mi euismod, venenatis justo id, imperdiet velit. Etiam eget nisi vel nisl placerat consectetur. Maecenas efficitur neque in nisi auctor efficitur. Aenean placerat diam aliquet mi rhoncus pharetra. Integer maximus sapien eget felis ultricies cursus. In ut mattis diam. Suspendisse sodales, massa a hendrerit sollicitudin, diam nibh volutpat leo, in semper ante magna a mauris. Aliquam id leo aliquet, elementum mi sed, consequat urna. Sed auctor, velit id consectetur tincidunt, sapien mi mattis lacus, quis placerat ligula nibh eget nunc. Aliquam erat volutpat. Sed interdum metus sed neque egestas, vel porta orci porttitor. Suspendisse fringilla nisl risus, id pellentesque purus pretium at.', 3, 'paris.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `idiomas`
--

CREATE TABLE IF NOT EXISTS `idiomas` (
`idIdioma` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `descripcion` text,
  `estatus` int(11) NOT NULL,
  `bandera` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `idiomas`
--

INSERT INTO `idiomas` (`idIdioma`, `nombre`, `descripcion`, `estatus`, `bandera`) VALUES
(1, 'Español', 'Idioma Español', 1, 'spain.png'),
(2, 'Ingles', 'Idioma Ingles', 1, 'england.png');

-- --------------------------------------------------------

--
-- Table structure for table `paises`
--

CREATE TABLE IF NOT EXISTS `paises` (
`idPais` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `bandera` varchar(100) DEFAULT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=491 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paises`
--

INSERT INTO `paises` (`idPais`, `idIdioma`, `nombre`, `bandera`, `estatus`) VALUES
(1, 1, 'Afganistán', ' ', 1),
(2, 1, 'Islas Gland', ' ', 1),
(3, 1, 'Albania', ' ', 1),
(4, 1, 'Alemania', ' ', 1),
(5, 1, 'Andorra', ' ', 1),
(6, 1, 'Angola', ' ', 1),
(7, 1, 'Anguilla', ' ', 1),
(8, 1, 'Antártida', ' ', 1),
(9, 1, 'Antigua y Barbuda', ' ', 1),
(10, 1, 'Antillas Holandesas', ' ', 1),
(11, 1, 'Arabia Saudí', ' ', 1),
(12, 1, 'Argelia', ' ', 1),
(13, 1, 'Argentina', ' ', 1),
(14, 1, 'Armenia', ' ', 1),
(15, 1, 'Aruba', ' ', 1),
(16, 1, 'Australia', ' ', 1),
(17, 1, 'Austria', ' ', 1),
(18, 1, 'Azerbaiyán', ' ', 1),
(19, 1, 'Bahamas', ' ', 1),
(20, 1, 'Bahréin', ' ', 1),
(21, 1, 'Bangladesh', ' ', 1),
(22, 1, 'Barbados', ' ', 1),
(23, 1, 'Bielorrusia', ' ', 1),
(24, 1, 'Bélgica', ' ', 1),
(25, 1, 'Belice', ' ', 1),
(26, 1, 'Benin', ' ', 1),
(27, 1, 'Bermudas', ' ', 1),
(28, 1, 'Bhután', ' ', 1),
(29, 1, 'Bolivia', ' ', 1),
(30, 1, 'Bosnia y Herzegovina', ' ', 1),
(31, 1, 'Botsuana', ' ', 1),
(32, 1, 'Isla Bouvet', ' ', 1),
(33, 1, 'Brasil', ' ', 1),
(34, 1, 'Brunéi', ' ', 1),
(35, 1, 'Bulgaria', ' ', 1),
(36, 1, 'Burkina Faso', ' ', 1),
(37, 1, 'Burundi', ' ', 1),
(38, 1, 'Cabo Verde', ' ', 1),
(39, 1, 'Islas Caimán', ' ', 1),
(40, 1, 'Camboya', ' ', 1),
(41, 1, 'Camerún', ' ', 1),
(42, 1, 'Canadá', ' ', 1),
(43, 1, 'República Centroafricana', ' ', 1),
(44, 1, 'Chad', ' ', 1),
(45, 1, 'República Checa', ' ', 1),
(46, 1, 'Chile', ' ', 1),
(47, 1, 'China', ' ', 1),
(48, 1, 'Chipre', ' ', 1),
(49, 1, 'Isla de Navidad', ' ', 1),
(50, 1, 'Ciudad del Vaticano', ' ', 1),
(51, 1, 'Islas Cocos', ' ', 1),
(52, 1, 'Colombia', ' ', 1),
(53, 1, 'Comoras', ' ', 1),
(54, 1, 'República Democrática del Congo', ' ', 1),
(55, 1, 'Congo', ' ', 1),
(56, 1, 'Islas Cook', ' ', 1),
(57, 1, 'Corea del Norte', ' ', 1),
(58, 1, 'Corea del Sur', ' ', 1),
(59, 1, 'Costa de Marfil', ' ', 1),
(60, 1, 'Costa Rica', ' ', 1),
(61, 1, 'Croacia', ' ', 1),
(62, 1, 'Cuba', ' ', 1),
(63, 1, 'Dinamarca', ' ', 1),
(64, 1, 'Dominica', ' ', 1),
(65, 1, 'República Dominicana', ' ', 1),
(66, 1, 'Ecuador', ' ', 1),
(67, 1, 'Egipto', ' ', 1),
(68, 1, 'El Salvador', ' ', 1),
(69, 1, 'Emiratos Árabes Unidos', ' ', 1),
(70, 1, 'Eritrea', ' ', 1),
(71, 1, 'Eslovaquia', ' ', 1),
(72, 1, 'Eslovenia', ' ', 1),
(73, 1, 'España', ' ', 1),
(74, 1, 'Islas ultramarinas de Estados Unidos', ' ', 1),
(75, 1, 'Estados Unidos', ' ', 1),
(76, 1, 'Estonia', ' ', 1),
(77, 1, 'Etiopía', ' ', 1),
(78, 1, 'Islas Feroe', ' ', 1),
(79, 1, 'Filipinas', ' ', 1),
(80, 1, 'Finlandia', ' ', 1),
(81, 1, 'Fiyi', ' ', 1),
(82, 1, 'Francia', 'france.png ', 1),
(83, 1, 'Gabón', ' ', 1),
(84, 1, 'Gambia', ' ', 1),
(85, 1, 'Georgia', ' ', 1),
(86, 1, 'Islas Georgias del Sur y Sandwich del Sur', ' ', 1),
(87, 1, 'Ghana', ' ', 1),
(88, 1, 'Gibraltar', ' ', 1),
(89, 1, 'Granada', ' ', 1),
(90, 1, 'Grecia', ' ', 1),
(91, 1, 'Groenlandia', ' ', 1),
(92, 1, 'Guadalupe', ' ', 1),
(93, 1, 'Guam', ' ', 1),
(94, 1, 'Guatemala', ' ', 1),
(95, 1, 'Guayana Francesa', ' ', 1),
(96, 1, 'Guinea', ' ', 1),
(97, 1, 'Guinea Ecuatorial', ' ', 1),
(98, 1, 'Guinea-Bissau', ' ', 1),
(99, 1, 'Guyana', ' ', 1),
(100, 1, 'Haití', ' ', 1),
(101, 1, 'Islas Heard y McDonald', ' ', 1),
(102, 1, 'Honduras', ' ', 1),
(103, 1, 'Hong Kong', ' ', 1),
(104, 1, 'Hungría', ' ', 1),
(105, 1, 'India', ' ', 1),
(106, 1, 'Indonesia', ' ', 1),
(107, 1, 'Irán', ' ', 1),
(108, 1, 'Iraq', ' ', 1),
(109, 1, 'Irlanda', ' ', 1),
(110, 1, 'Islandia', ' ', 1),
(111, 1, 'Israel', ' ', 1),
(112, 1, 'Italia', ' ', 1),
(113, 1, 'Jamaica', ' ', 1),
(114, 1, 'Japón', ' ', 1),
(115, 1, 'Jordania', ' ', 1),
(116, 1, 'Kazajstán', ' ', 1),
(117, 1, 'Kenia', ' ', 1),
(118, 1, 'Kirguistán', ' ', 1),
(119, 1, 'Kiribati', ' ', 1),
(120, 1, 'Kuwait', ' ', 1),
(121, 1, 'Laos', ' ', 1),
(122, 1, 'Lesotho', ' ', 1),
(123, 1, 'Letonia', ' ', 1),
(124, 1, 'Líbano', ' ', 1),
(125, 1, 'Liberia', ' ', 1),
(126, 1, 'Libia', ' ', 1),
(127, 1, 'Liechtenstein', ' ', 1),
(128, 1, 'Lituania', ' ', 1),
(129, 1, 'Luxemburgo', ' ', 1),
(130, 1, 'Macao', ' ', 1),
(131, 1, 'ARY Macedonia', ' ', 1),
(132, 1, 'Madagascar', ' ', 1),
(133, 1, 'Malasia', ' ', 1),
(134, 1, 'Malawi', ' ', 1),
(135, 1, 'Maldivas', ' ', 1),
(136, 1, 'Malí', ' ', 1),
(137, 1, 'Malta', ' ', 1),
(138, 1, 'Islas Malvinas', ' ', 1),
(139, 1, 'Islas Marianas del Norte', ' ', 1),
(140, 1, 'Marruecos', ' ', 1),
(141, 1, 'Islas Marshall', ' ', 1),
(142, 1, 'Martinica', ' ', 1),
(143, 1, 'Mauricio', ' ', 1),
(144, 1, 'Mauritania', ' ', 1),
(145, 1, 'Mayotte', ' ', 1),
(146, 1, 'México', ' ', 1),
(147, 1, 'Micronesia', ' ', 1),
(148, 1, 'Moldavia', ' ', 1),
(149, 1, 'Mónaco', ' ', 1),
(150, 1, 'Mongolia', ' ', 1),
(151, 1, 'Montserrat', ' ', 1),
(152, 1, 'Mozambique', ' ', 1),
(153, 1, 'Myanmar', ' ', 1),
(154, 1, 'Namibia', ' ', 1),
(155, 1, 'Nauru', ' ', 1),
(156, 1, 'Nepal', ' ', 1),
(157, 1, 'Nicaragua', ' ', 1),
(158, 1, 'Níger', ' ', 1),
(159, 1, 'Nigeria', ' ', 1),
(160, 1, 'Niue', ' ', 1),
(161, 1, 'Isla Norfolk', ' ', 1),
(162, 1, 'Noruega', ' ', 1),
(163, 1, 'Nueva Caledonia', ' ', 1),
(164, 1, 'Nueva Zelanda', ' ', 1),
(165, 1, 'Omán', ' ', 1),
(166, 1, 'Países Bajos', ' ', 1),
(167, 1, 'Pakistán', ' ', 1),
(168, 1, 'Palau', ' ', 1),
(169, 1, 'Palestina', ' ', 1),
(170, 1, 'Panamá', ' ', 1),
(171, 1, 'Papúa Nueva Guinea', ' ', 1),
(172, 1, 'Paraguay', ' ', 1),
(173, 1, 'Perú', ' ', 1),
(174, 1, 'Islas Pitcairn', ' ', 1),
(175, 1, 'Polinesia Francesa', ' ', 1),
(176, 1, 'Polonia', ' ', 1),
(177, 1, 'Portugal', ' ', 1),
(178, 1, 'Puerto Rico', ' ', 1),
(179, 1, 'Qatar', ' ', 1),
(180, 1, 'Reino Unido', ' ', 1),
(181, 1, 'Reunión', ' ', 1),
(182, 1, 'Ruanda', ' ', 1),
(183, 1, 'Rumania', ' ', 1),
(184, 1, 'Rusia', ' ', 1),
(185, 1, 'Sahara Occidental', ' ', 1),
(186, 1, 'Islas Salomón', ' ', 1),
(187, 1, 'Samoa', ' ', 1),
(188, 1, 'Samoa Americana', ' ', 1),
(189, 1, 'San Cristóbal y Nevis', ' ', 1),
(190, 1, 'San Marino', ' ', 1),
(191, 1, 'San Pedro y Miquelón', ' ', 1),
(192, 1, 'San Vicente y las Granadinas', ' ', 1),
(193, 1, 'Santa Helena', ' ', 1),
(194, 1, 'Santa Lucía', ' ', 1),
(195, 1, 'Santo Tomé y Príncipe', ' ', 1),
(196, 1, 'Senegal', ' ', 1),
(197, 1, 'Serbia y Montenegro', ' ', 1),
(198, 1, 'Seychelles', ' ', 1),
(199, 1, 'Sierra Leona', ' ', 1),
(200, 1, 'Singapur', ' ', 1),
(201, 1, 'Siria', ' ', 1),
(202, 1, 'Somalia', ' ', 1),
(203, 1, 'Sri Lanka', ' ', 1),
(204, 1, 'Suazilandia', ' ', 1),
(205, 1, 'Sudáfrica', ' ', 1),
(206, 1, 'Sudán', ' ', 1),
(207, 1, 'Suecia', ' ', 1),
(208, 1, 'Suiza', ' ', 1),
(209, 1, 'Surinam', ' ', 1),
(210, 1, 'Svalbard y Jan Mayen', ' ', 1),
(211, 1, 'Tailandia', ' ', 1),
(212, 1, 'Taiwán', ' ', 1),
(213, 1, 'Tanzania', ' ', 1),
(214, 1, 'Tayikistán', ' ', 1),
(215, 1, 'Territorio Británico del Océano Índico', ' ', 1),
(216, 1, 'Territorios Australes Franceses', ' ', 1),
(217, 1, 'Timor Oriental', ' ', 1),
(218, 1, 'Togo', ' ', 1),
(219, 1, 'Tokelau', ' ', 1),
(220, 1, 'Tonga', ' ', 1),
(221, 1, 'Trinidad y Tobago', ' ', 1),
(222, 1, 'Túnez', ' ', 1),
(223, 1, 'Islas Turcas y Caicos', ' ', 1),
(224, 1, 'Turkmenistán', ' ', 1),
(225, 1, 'Turquía', ' ', 1),
(226, 1, 'Tuvalu', ' ', 1),
(227, 1, 'Ucrania', ' ', 1),
(228, 1, 'Uganda', ' ', 1),
(229, 1, 'Uruguay', ' ', 1),
(230, 1, 'Uzbekistán', ' ', 1),
(231, 1, 'Vanuatu', ' ', 1),
(232, 1, 'Venezuela', ' ', 1),
(233, 1, 'Vietnam', ' ', 1),
(234, 1, 'Islas Vírgenes Británicas', ' ', 1),
(235, 1, 'Islas Vírgenes de los Estados Unidos', ' ', 1),
(236, 1, 'Wallis y Futuna', ' ', 1),
(237, 1, 'Yemen', ' ', 1),
(238, 1, 'Yibuti', ' ', 1),
(239, 1, 'Zambia', ' ', 1),
(240, 1, 'Zimbabue', ' ', 1),
(241, 2, 'Australia', ' ', 1),
(242, 2, 'China', ' ', 1),
(243, 2, 'Japan', ' ', 1),
(244, 2, 'Thailand', ' ', 1),
(245, 2, 'India', ' ', 1),
(246, 2, 'Malaysia', ' ', 1),
(247, 2, 'Kore', ' ', 1),
(248, 2, 'Hong Kong', ' ', 1),
(249, 2, 'Taiwan', ' ', 1),
(250, 2, 'Philippines', ' ', 1),
(251, 2, 'Vietnam', ' ', 1),
(252, 2, 'France', ' ', 1),
(253, 2, 'Germany', ' ', 1),
(254, 2, 'Sweden', ' ', 1),
(255, 2, 'Italy', ' ', 1),
(256, 2, 'Greece', ' ', 1),
(257, 2, 'Spain', ' ', 1),
(258, 2, 'Austria', ' ', 1),
(259, 2, 'United Kingdom', ' ', 1),
(260, 2, 'Netherlands', ' ', 1),
(261, 2, 'Belgium', ' ', 1),
(262, 2, 'Switzerland', ' ', 1),
(263, 2, 'United Arab Emirates', ' ', 1),
(264, 2, 'Israel', ' ', 1),
(265, 2, 'Ukraine', ' ', 1),
(266, 2, 'Russian Federation', ' ', 1),
(267, 2, 'Kazakhstan', ' ', 1),
(268, 2, 'Portugal', ' ', 1),
(269, 2, 'Saudi Arabia', ' ', 1),
(270, 2, 'Denmark', ' ', 1),
(271, 2, 'Ira', ' ', 1),
(272, 2, 'Norway', ' ', 1),
(273, 2, 'United States', ' ', 1),
(274, 2, 'Mexico', ' ', 1),
(275, 2, 'Canada', ' ', 1),
(276, 2, 'Anonymous Proxy', ' ', 1),
(277, 2, 'Syrian Arab Republic', ' ', 1),
(278, 2, 'Cyprus', ' ', 1),
(279, 2, 'Czech Republic', ' ', 1),
(280, 2, 'Iraq', ' ', 1),
(281, 2, 'Turkey', ' ', 1),
(282, 2, 'Romania', ' ', 1),
(283, 2, 'Lebanon', ' ', 1),
(284, 2, 'Hungary', ' ', 1),
(285, 2, 'Georgia', ' ', 1),
(286, 2, 'Brazil', ' ', 1),
(287, 2, 'Azerbaijan', ' ', 1),
(288, 2, 'Satellite Provider', ' ', 1),
(289, 2, 'Palestinian Territory', ' ', 1),
(290, 2, 'Lithuania', ' ', 1),
(291, 2, 'Oman', ' ', 1),
(292, 2, 'Slovakia', ' ', 1),
(293, 2, 'Serbia', ' ', 1),
(294, 2, 'Finland', ' ', 1),
(295, 2, 'Iceland', ' ', 1),
(296, 2, 'Bulgaria', ' ', 1),
(297, 2, 'Slovenia', ' ', 1),
(298, 2, 'Moldov', ' ', 1),
(299, 2, 'Macedonia', ' ', 1),
(300, 2, 'Liechtenstein', ' ', 1),
(301, 2, 'Jersey', ' ', 1),
(302, 2, 'Poland', ' ', 1),
(303, 2, 'Croatia', ' ', 1),
(304, 2, 'Bosnia and Herzegovina', ' ', 1),
(305, 2, 'Estonia', ' ', 1),
(306, 2, 'Latvia', ' ', 1),
(307, 2, 'Jordan', ' ', 1),
(308, 2, 'Kyrgyzstan', ' ', 1),
(309, 2, 'Reunion', ' ', 1),
(310, 2, 'Ireland', ' ', 1),
(311, 2, 'Libya', ' ', 1),
(312, 2, 'Luxembourg', ' ', 1),
(313, 2, 'Armenia', ' ', 1),
(314, 2, 'Virgin Island', ' ', 1),
(315, 2, 'Yemen', ' ', 1),
(316, 2, 'Belarus', ' ', 1),
(317, 2, 'Gibraltar', ' ', 1),
(318, 2, 'Martinique', ' ', 1),
(319, 2, 'Panama', ' ', 1),
(320, 2, 'Dominican Republic', ' ', 1),
(321, 2, 'Guam', ' ', 1),
(322, 2, 'Puerto Rico', ' ', 1),
(323, 2, 'Virgin Island', ' ', 1),
(324, 2, 'Mongolia', ' ', 1),
(325, 2, 'New Zealand', ' ', 1),
(326, 2, 'Singapore', ' ', 1),
(327, 2, 'Indonesia', ' ', 1),
(328, 2, 'Nepal', ' ', 1),
(329, 2, 'Papua New Guinea', ' ', 1),
(330, 2, 'Pakistan', ' ', 1),
(331, 2, 'Asia/Pacific Region', ' ', 1),
(332, 2, 'Bahamas', ' ', 1),
(333, 2, 'Saint Lucia', ' ', 1),
(334, 2, 'Argentina', ' ', 1),
(335, 2, 'Bangladesh', ' ', 1),
(336, 2, 'Tokelau', ' ', 1),
(337, 2, 'Cambodia', ' ', 1),
(338, 2, 'Macau', ' ', 1),
(339, 2, 'Maldives', ' ', 1),
(340, 2, 'Afghanistan', ' ', 1),
(341, 2, 'New Caledonia', ' ', 1),
(342, 2, 'Fiji', ' ', 1),
(343, 2, 'Wallis and Futuna', ' ', 1),
(344, 2, 'Qatar', ' ', 1),
(345, 2, 'Albania', ' ', 1),
(346, 2, 'Belize', ' ', 1),
(347, 2, 'Uzbekistan', ' ', 1),
(348, 2, 'Kuwait', ' ', 1),
(349, 2, 'Montenegro', ' ', 1),
(350, 2, 'Peru', ' ', 1),
(351, 2, 'Bermuda', ' ', 1),
(352, 2, 'Curacao', ' ', 1),
(353, 2, 'Colombia', ' ', 1),
(354, 2, 'Venezuela', ' ', 1),
(355, 2, 'Chile', ' ', 1),
(356, 2, 'Ecuador', ' ', 1),
(357, 2, 'South Africa', ' ', 1),
(358, 2, 'Isle of Man', ' ', 1),
(359, 2, 'Bolivia', ' ', 1),
(360, 2, 'Guernsey', ' ', 1),
(361, 2, 'Malta', ' ', 1),
(362, 2, 'Tajikistan', ' ', 1),
(363, 2, 'Seychelles', ' ', 1),
(364, 2, 'Bahrain', ' ', 1),
(365, 2, 'Egypt', ' ', 1),
(366, 2, 'Zimbabwe', ' ', 1),
(367, 2, 'Liberia', ' ', 1),
(368, 2, 'Kenya', ' ', 1),
(369, 2, 'Ghana', ' ', 1),
(370, 2, 'Nigeria', ' ', 1),
(371, 2, 'Tanzani', ' ', 1),
(372, 2, 'Zambia', ' ', 1),
(373, 2, 'Madagascar', ' ', 1),
(374, 2, 'Angola', ' ', 1),
(375, 2, 'Namibia', ' ', 1),
(376, 2, 'Cote DIvoire', ' ', 1),
(377, 2, 'Sudan', ' ', 1),
(378, 2, 'Cameroon', ' ', 1),
(379, 2, 'Malawi', ' ', 1),
(380, 2, 'Gabon', ' ', 1),
(381, 2, 'Mali', ' ', 1),
(382, 2, 'Benin', ' ', 1),
(383, 2, 'Chad', ' ', 1),
(384, 2, 'Botswana', ' ', 1),
(385, 2, 'Cape Verde', ' ', 1),
(386, 2, 'Rwanda', ' ', 1),
(387, 2, 'Congo', ' ', 1),
(388, 2, 'Uganda', ' ', 1),
(389, 2, 'Mozambique', ' ', 1),
(390, 2, 'Gambia', ' ', 1),
(391, 2, 'Lesotho', ' ', 1),
(392, 2, 'Mauritius', ' ', 1),
(393, 2, 'Morocco', ' ', 1),
(394, 2, 'Algeria', ' ', 1),
(395, 2, 'Guinea', ' ', 1),
(396, 2, 'Cong', ' ', 1),
(397, 2, 'Swaziland', ' ', 1),
(398, 2, 'Burkina Faso', ' ', 1),
(399, 2, 'Sierra Leone', ' ', 1),
(400, 2, 'Somalia', ' ', 1),
(401, 2, 'Niger', ' ', 1),
(402, 2, 'Central African Republic', ' ', 1),
(403, 2, 'Togo', ' ', 1),
(404, 2, 'Burundi', ' ', 1),
(405, 2, 'Equatorial Guinea', ' ', 1),
(406, 2, 'South Sudan', ' ', 1),
(407, 2, 'Senegal', ' ', 1),
(408, 2, 'Mauritania', ' ', 1),
(409, 2, 'Djibouti', ' ', 1),
(410, 2, 'Comoros', ' ', 1),
(411, 2, 'British Indian Ocean Territory', ' ', 1),
(412, 2, 'Tunisia', ' ', 1),
(413, 2, 'Greenland', ' ', 1),
(414, 2, 'Holy See (Vatican City State)', ' ', 1),
(415, 2, 'Costa Rica', ' ', 1),
(416, 2, 'Cayman Islands', ' ', 1),
(417, 2, 'Jamaica', ' ', 1),
(418, 2, 'Guatemala', ' ', 1),
(419, 2, 'Marshall Islands', ' ', 1),
(420, 2, 'Antarctica', ' ', 1),
(421, 2, 'Barbados', ' ', 1),
(422, 2, 'Aruba', ' ', 1),
(423, 2, 'Monaco', ' ', 1),
(424, 2, 'Anguilla', ' ', 1),
(425, 2, 'Saint Kitts and Nevis', ' ', 1),
(426, 2, 'Grenada', ' ', 1),
(427, 2, 'Paraguay', ' ', 1),
(428, 2, 'Montserrat', ' ', 1),
(429, 2, 'Turks and Caicos Islands', ' ', 1),
(430, 2, 'Antigua and Barbuda', ' ', 1),
(431, 2, 'Tuvalu', ' ', 1),
(432, 2, 'French Polynesia', ' ', 1),
(433, 2, 'Solomon Islands', ' ', 1),
(434, 2, 'Vanuatu', ' ', 1),
(435, 2, 'Eritrea', ' ', 1),
(436, 2, 'Trinidad and Tobago', ' ', 1),
(437, 2, 'Andorra', ' ', 1),
(438, 2, 'Haiti', ' ', 1),
(439, 2, 'Saint Helena', ' ', 1),
(440, 2, 'Micronesi', ' ', 1),
(441, 2, 'El Salvador', ' ', 1),
(442, 2, 'Honduras', ' ', 1),
(443, 2, 'Uruguay', ' ', 1),
(444, 2, 'Sri Lanka', ' ', 1),
(445, 2, 'Western Sahara', ' ', 1),
(446, 2, 'Christmas Island', ' ', 1),
(447, 2, 'Samoa', ' ', 1),
(448, 2, 'Suriname', ' ', 1),
(449, 2, 'Cook Islands', ' ', 1),
(450, 2, 'Kiribati', ' ', 1),
(451, 2, 'Niue', ' ', 1),
(452, 2, 'Tonga', ' ', 1),
(453, 2, 'French Southern Territories', ' ', 1),
(454, 2, 'Mayotte', ' ', 1),
(455, 2, 'Norfolk Island', ' ', 1),
(456, 2, 'Brunei Darussalam', ' ', 1),
(457, 2, 'Turkmenistan', ' ', 1),
(458, 2, 'Pitcairn Islands', ' ', 1),
(459, 2, 'San Marino', ' ', 1),
(460, 2, 'Aland Islands', ' ', 1),
(461, 2, 'Faroe Islands', ' ', 1),
(462, 2, 'Svalbard and Jan Mayen', ' ', 1),
(463, 2, 'Cocos (Keeling) Islands', ' ', 1),
(464, 2, 'Nauru', ' ', 1),
(465, 2, 'South Georgia and the South Sandwich Islands', ' ', 1),
(466, 2, 'United States Minor Outlying Islands', ' ', 1),
(467, 2, 'Guinea-Bissau', ' ', 1),
(468, 2, 'Palau', ' ', 1),
(469, 2, 'American Samoa', ' ', 1),
(470, 2, 'Bhutan', ' ', 1),
(471, 2, 'French Guiana', ' ', 1),
(472, 2, 'Guadeloupe', ' ', 1),
(473, 2, 'Saint Martin', ' ', 1),
(474, 2, 'Saint Vincent and the Grenadines', ' ', 1),
(475, 2, 'Saint Pierre and Miquelon', ' ', 1),
(476, 2, 'Saint Barthelemy', ' ', 1),
(477, 2, 'Dominica', ' ', 1),
(478, 2, 'Sao Tome and Principe', ' ', 1),
(479, 2, 'Kore', ' ', 1),
(480, 2, 'Falkland Islands (Malvinas)', ' ', 1),
(481, 2, 'Northern Mariana Islands', ' ', 1),
(482, 2, 'Timor-Leste', ' ', 1),
(483, 2, 'Bonair', ' ', 1),
(484, 2, 'Myanmar', ' ', 1),
(485, 2, 'Nicaragua', ' ', 1),
(486, 2, 'Sint Maarten (Dutch part)', ' ', 1),
(487, 2, 'Guyana', ' ', 1),
(488, 2, 'Lao Peoples Democratic Republic', ' ', 1),
(489, 2, 'Cuba', ' ', 1),
(490, 2, 'Ethiopia', ' ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `paquetes`
--

CREATE TABLE IF NOT EXISTS `paquetes` (
`idPaquete` int(11) NOT NULL,
  `idPais` int(11) NOT NULL,
  `idCiudad` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `idioma` varchar(20) NOT NULL,
  `etiqueta` varchar(50) DEFAULT NULL,
  `precio` text NOT NULL,
  `descripcion` text NOT NULL,
  `itinerario` text,
  `puntoEcuentro` text,
  `descripcionDuracion` text,
  `adicional` text,
  `preguntasFrecuentes` text,
  `fecha` text NOT NULL,
  `seccionVIP` text
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paquetes`
--

INSERT INTO `paquetes` (`idPaquete`, `idPais`, `idCiudad`, `idIdioma`, `titulo`, `idioma`, `etiqueta`, `precio`, `descripcion`, `itinerario`, `puntoEcuentro`, `descripcionDuracion`, `adicional`, `preguntasFrecuentes`, `fecha`, `seccionVIP`) VALUES
(1, 112, 1, 1, 'Paquete asdkjlasdlk', 'Espanol', 'asdasd', '{\n"tipoActividad":[{"Titulo":"Adultos","Precio":40},{"Titulo":"Niños de 3 a 11 años","Precio":30}],\n"Titulo":"Niños menos de 3 años",\n"Precio":"Gratis"\n}', 'asdasdasdas', '121212', '12121', '21212', '21212', '[{\n"Pregunta":"¿Qué puedo hacer si no llego a tiempo?",\n"Respuesta":"respuesta de la pregunta 1"\n},\n{"Pregunta":"¿Como hago una reserva?","Respuesta":"respuesta de la pregunta 2"}]', '[{"Cupo": 60, "Fecha": "11/09/2018", "Horario": "09:00"}, {"Cupo": 60, "Fecha": "11/09/2018", "Horario": "13:00"}, {"Cupo": 60, "Fecha": "12/09/2018", "Horario": "09:00"}]', '[{"Titulo":"Este es el titulo","Texto":"aqui va un texto referente"},{"Titulo":"Este es el titulo","Texto":"aqui va un texto referente"}]'),
(4, 4, 2, 1, 'Paquete 1', 'Español', 'Descuento 50%', '{\n"tipoActividad":[{"Titulo":"Adultos","Precio":40},{"Titulo":"Niños de 3 a 11 años","Precio":30}],\n"Titulo":"Niños menos de 3 años",\n"Precio":"Gratis"\n}', 'Prueba', 'Prueba', 'Prueba', 'Prueba', 'Prueba', '[{\n"Pregunta":"¿Qué puedo hacer si no llego a tiempo?",\n"Respuesta":"respuesta de la pregunta 1"\n},\n{"Pregunta":"¿Como hago una reserva?","Respuesta":"respuesta de la pregunta 2"}]', '[{"Cupo": 60, "Fecha": "11/09/2018", "Horario": "09:00"}, {"Cupo": 60, "Fecha": "11/09/2018", "Horario": "13:00"}, {"Cupo": 60, "Fecha": "12/09/2018", "Horario": "09:00"}]', '[{"Titulo":"Este es el titulo","Texto":"aqui va un texto referente"},{"Titulo":"Este es el titulo","Texto":"aqui va un texto referente"}]');

-- --------------------------------------------------------

--
-- Table structure for table `paquetesguias`
--

CREATE TABLE IF NOT EXISTS `paquetesguias` (
`idPG` int(11) NOT NULL,
  `idGuia` int(11) NOT NULL,
  `idPaquete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rutas`
--

CREATE TABLE IF NOT EXISTS `rutas` (
`id` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `seccion` varchar(20) NOT NULL,
  `ruta` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rutas`
--

INSERT INTO `rutas` (`id`, `idIdioma`, `nombre`, `seccion`, `ruta`) VALUES
(1, 1, 'Quienes Somos', 'qSomos', 'quienesSomos/es'),
(2, 2, 'About Us', 'qSomos', 'aboutUs/en'),
(3, 1, 'Destinos', 'destinos', 'destinos/es'),
(4, 2, 'Trips', 'destinos', 'trips/en'),
(5, 1, 'Guias', 'guias', 'guias/es'),
(6, 2, 'Guides', 'guias', 'guides/en'),
(7, 1, 'Cotizador', 'cotizador', 'cotizador/es'),
(8, 2, 'Quoting', 'cotizador', 'quoting/en'),
(9, 1, 'Contacto', 'contacto', 'contacto/es'),
(10, 2, 'Contact', 'contacto', 'contact/en'),
(11, 1, 'Inicie Sesión', 'sesion', ' '),
(12, 2, 'Log In', 'sesion', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `seccionesparallax`
--

CREATE TABLE IF NOT EXISTS `seccionesparallax` (
`idSeccionP` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `subtitulo` varchar(100) DEFAULT NULL,
  `seccion` varchar(20) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seccionesparallax`
--

INSERT INTO `seccionesparallax` (`idSeccionP`, `idIdioma`, `nombre`, `titulo`, `subtitulo`, `seccion`, `imagen`, `estatus`) VALUES
(1, 1, 'Quienes Somos', 'Quienes Somos', 'Subtitulo quienes somos', 'qsomos', 'imagen.jpg', 1),
(2, 1, 'Contacto', 'Titulo de contactos', ' ', 'contacto', 'imagen.jpg', 1),
(3, 1, 'Guias', 'Contamos con los mejores guías', 'Vivirás experiencias increibles en tu tours', 'guias', 'guias.jpg', 1),
(4, 2, 'Guias', 'We have the best guides ', 'You will live incredible experiences in our tours', 'guias', 'guias.jpg', 1),
(5, 2, 'About Us', 'About Us', 'sub tittle of About us', 'qsomos', 'imagen.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
`idSlider` int(11) NOT NULL,
  `idIdioma` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `subtitulo` varchar(100) DEFAULT NULL,
  `imagen` varchar(150) NOT NULL,
  `estatus` int(11) NOT NULL,
  `seccion` varchar(20) NOT NULL,
  `boton` text,
  `tipo` varchar(20) DEFAULT NULL,
  `orientacion` varchar(50) DEFAULT NULL,
  `bcolor` varchar(20) DEFAULT NULL,
  `descripcion` text
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`idSlider`, `idIdioma`, `titulo`, `subtitulo`, `imagen`, `estatus`, `seccion`, `boton`, `tipo`, `orientacion`, `bcolor`, `descripcion`) VALUES
(1, 1, 'prueba', 'prueba', 'imagen4.jpg', 1, 'home', '{"nombre":"Cotizame","url":"cotizador/es"}', NULL, 'h', '#000', 'sdfsdfdsfsdfdsf'),
(3, 2, 'Test', 'Test', 'imagen2.jpg', 1, 'home', '{"nombre":"Quote Me","url":"quoting/en"}', NULL, 'h', '#000', 'asdasdasdsad'),
(4, 1, 'Prueba II', 'Prueba II', 'imagen3.jpg', 1, 'home', '', NULL, 'h', '#000', 'sdfsdfsdfsd');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`idUsuario` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `tipo` int(11) NOT NULL,
  `metodoRegistro` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carritos`
--
ALTER TABLE `carritos`
 ADD PRIMARY KEY (`idCarrito`), ADD KEY `idIdioma` (`idIdioma`), ADD KEY `idPaquete` (`idPaquete`);

--
-- Indexes for table `ciudades`
--
ALTER TABLE `ciudades`
 ADD PRIMARY KEY (`idCiudad`), ADD KEY `idPais` (`idPais`);

--
-- Indexes for table `contactos`
--
ALTER TABLE `contactos`
 ADD PRIMARY KEY (`idContacto`), ADD KEY `idIdioma` (`idIdioma`);

--
-- Indexes for table `empresas`
--
ALTER TABLE `empresas`
 ADD PRIMARY KEY (`idEmpresa`), ADD KEY `idIdioma` (`idIdioma`);

--
-- Indexes for table `encuestas`
--
ALTER TABLE `encuestas`
 ADD PRIMARY KEY (`idEncuesta`), ADD KEY `idUsuario` (`idUsuario`);

--
-- Indexes for table `equipo`
--
ALTER TABLE `equipo`
 ADD PRIMARY KEY (`idEquipo`), ADD KEY `idIdioma` (`idIdioma`);

--
-- Indexes for table `evaluaciones`
--
ALTER TABLE `evaluaciones`
 ADD PRIMARY KEY (`idEvaluacion`), ADD KEY `idGuia` (`idGuia`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
 ADD PRIMARY KEY (`idFooter`), ADD KEY `idIdioma` (`idIdioma`);

--
-- Indexes for table `formularios`
--
ALTER TABLE `formularios`
 ADD PRIMARY KEY (`idFormulario`), ADD KEY `idIdioma` (`idIdioma`), ADD KEY `idPais` (`idPais`);

--
-- Indexes for table `guias`
--
ALTER TABLE `guias`
 ADD PRIMARY KEY (`idGuia`), ADD KEY `idIdioma` (`idIdioma`), ADD KEY `idCiudad` (`idCiudad`);

--
-- Indexes for table `idiomas`
--
ALTER TABLE `idiomas`
 ADD PRIMARY KEY (`idIdioma`);

--
-- Indexes for table `paises`
--
ALTER TABLE `paises`
 ADD PRIMARY KEY (`idPais`), ADD KEY `idIdioma` (`idIdioma`);

--
-- Indexes for table `paquetes`
--
ALTER TABLE `paquetes`
 ADD PRIMARY KEY (`idPaquete`), ADD KEY `idIdioma` (`idIdioma`), ADD KEY `idPais` (`idPais`), ADD KEY `idCiudad` (`idCiudad`);

--
-- Indexes for table `paquetesguias`
--
ALTER TABLE `paquetesguias`
 ADD PRIMARY KEY (`idPG`), ADD KEY `idGuia` (`idGuia`), ADD KEY `idPaquete` (`idPaquete`);

--
-- Indexes for table `rutas`
--
ALTER TABLE `rutas`
 ADD PRIMARY KEY (`id`), ADD KEY `idIdioma` (`idIdioma`);

--
-- Indexes for table `seccionesparallax`
--
ALTER TABLE `seccionesparallax`
 ADD PRIMARY KEY (`idSeccionP`), ADD KEY `idIdioma` (`idIdioma`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
 ADD PRIMARY KEY (`idSlider`), ADD KEY `idIdioma` (`idIdioma`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carritos`
--
ALTER TABLE `carritos`
MODIFY `idCarrito` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ciudades`
--
ALTER TABLE `ciudades`
MODIFY `idCiudad` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contactos`
--
ALTER TABLE `contactos`
MODIFY `idContacto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `empresas`
--
ALTER TABLE `empresas`
MODIFY `idEmpresa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `encuestas`
--
ALTER TABLE `encuestas`
MODIFY `idEncuesta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `equipo`
--
ALTER TABLE `equipo`
MODIFY `idEquipo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `evaluaciones`
--
ALTER TABLE `evaluaciones`
MODIFY `idEvaluacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
MODIFY `idFooter` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `formularios`
--
ALTER TABLE `formularios`
MODIFY `idFormulario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `guias`
--
ALTER TABLE `guias`
MODIFY `idGuia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `idiomas`
--
ALTER TABLE `idiomas`
MODIFY `idIdioma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `paises`
--
ALTER TABLE `paises`
MODIFY `idPais` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=491;
--
-- AUTO_INCREMENT for table `paquetes`
--
ALTER TABLE `paquetes`
MODIFY `idPaquete` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `paquetesguias`
--
ALTER TABLE `paquetesguias`
MODIFY `idPG` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rutas`
--
ALTER TABLE `rutas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `seccionesparallax`
--
ALTER TABLE `seccionesparallax`
MODIFY `idSeccionP` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
MODIFY `idSlider` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `carritos`
--
ALTER TABLE `carritos`
ADD CONSTRAINT `carritos_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `carritos_ibfk_2` FOREIGN KEY (`idPaquete`) REFERENCES `paquetes` (`idPaquete`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ciudades`
--
ALTER TABLE `ciudades`
ADD CONSTRAINT `ciudades_ibfk_1` FOREIGN KEY (`idPais`) REFERENCES `paises` (`idPais`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contactos`
--
ALTER TABLE `contactos`
ADD CONSTRAINT `contactos_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `empresas`
--
ALTER TABLE `empresas`
ADD CONSTRAINT `empresas_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `encuestas`
--
ALTER TABLE `encuestas`
ADD CONSTRAINT `encuestas_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `equipo`
--
ALTER TABLE `equipo`
ADD CONSTRAINT `equipo_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluaciones`
--
ALTER TABLE `evaluaciones`
ADD CONSTRAINT `evaluaciones_ibfk_1` FOREIGN KEY (`idGuia`) REFERENCES `guias` (`idGuia`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `footer`
--
ALTER TABLE `footer`
ADD CONSTRAINT `footer_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `formularios`
--
ALTER TABLE `formularios`
ADD CONSTRAINT `formularios_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `formularios_ibfk_2` FOREIGN KEY (`idPais`) REFERENCES `paises` (`idPais`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `guias`
--
ALTER TABLE `guias`
ADD CONSTRAINT `guias_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `guias_ibfk_2` FOREIGN KEY (`idCiudad`) REFERENCES `ciudades` (`idCiudad`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paises`
--
ALTER TABLE `paises`
ADD CONSTRAINT `paises_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paquetes`
--
ALTER TABLE `paquetes`
ADD CONSTRAINT `paquetes_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `paquetes_ibfk_2` FOREIGN KEY (`idPais`) REFERENCES `paises` (`idPais`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `paquetes_ibfk_3` FOREIGN KEY (`idCiudad`) REFERENCES `ciudades` (`idCiudad`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paquetesguias`
--
ALTER TABLE `paquetesguias`
ADD CONSTRAINT `paquetesguias_ibfk_1` FOREIGN KEY (`idGuia`) REFERENCES `guias` (`idGuia`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `paquetesguias_ibfk_2` FOREIGN KEY (`idPaquete`) REFERENCES `paquetes` (`idPaquete`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rutas`
--
ALTER TABLE `rutas`
ADD CONSTRAINT `rutas_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seccionesparallax`
--
ALTER TABLE `seccionesparallax`
ADD CONSTRAINT `seccionesparallax_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sliders`
--
ALTER TABLE `sliders`
ADD CONSTRAINT `sliders_ibfk_1` FOREIGN KEY (`idIdioma`) REFERENCES `idiomas` (`idIdioma`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
