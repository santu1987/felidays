create table equipo(
	idEquipo int not null primary key auto_increment,
    idIdioma int not null,
    nombre varchar(60) not null,
    funcion varchar(60) not null,
    descripcion text not null,
    foto varchar(100) not null,
    foreign key (idIdioma) references idiomas(idIdioma) on delete cascade on update cascade
);

INSERT INTO `equipo` VALUES (1,1,'Humberto Parisi','CEO & Fundador de SoftParDay','Humberto es informatico graduado en el IUJO, desde joven ha practicado el desarrollo de software entre otras actividades referentes a la informatica','guia1.jpg'),(2,1,'Daysi Rodriguez','CEO & Fundadora de Felidays','Daysi es abogada, especializada en derecho laborarl, pero ademas le encanta viajar y conocer nuevos destinos, lo cual hizo que tuviese una vision de crear un portal dedicado al turismo','guia2.jpg'),(3,1,'Astrid Feliciani','CEO & Fundadora de Felidays','Astrid es abogada, especializada en derecho laborarl, pero ademas le encanta viajar y conocer nuevos destinos, lo cual hizo que tuviese una vision de crear un portal dedicado al turismo','guia2.jpg');
