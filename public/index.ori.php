<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title></title>
<meta name="description" content="">
<meta name="keywords" content="">

    <meta name="author" content="">
    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <meta http-equiv="x-ua-compatible" content="IE=9">
    <!-- Font Awesome -->
    <link href="stylesheets/font-awesome.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="stylesheets/assets.min9e70.css?_build=1534856391" />
    <link rel="stylesheet" href="stylesheets/stylesa084.css?_build=1535590879" />

    <link rel="stylesheet" href="stylesheets/styles2faa.css?_build=1534859320" id="moto-website-style" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="stylesheets/menu.css">
   <!-- <link rel="stylesheet" href="stylesheets/flat-ui-slider.css"> -->
    <link rel="stylesheet" href="stylesheets/base.css"> 
    <link rel="stylesheet" href="stylesheets/skeleton.css">
    <link rel="stylesheet" href="stylesheets/landings.css">
    <link rel="stylesheet" href="stylesheets/main.css">
    <link rel="stylesheet" href="stylesheets/landings_layouts.css">
    <link rel="stylesheet" href="stylesheets/box.css">
    <link rel="stylesheet" href="stylesheets/pixicon.css">
    <link rel="stylesheet" href="stylesheets/framework.css">
    <link rel="stylesheet" href="stylesheets/carrousel.css">
    <link href="assets/css/animations.min.css" rel="stylesheet" type="text/css" media="all" /> 

    <link rel="stylesheet" href="stylesheets/estilos.css">
    <!-- <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css"> -->
    <link href="assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
    
    
   <!--  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    
</head>
<body>
    

<div class="page" id="page">
<div class="header_nav_1 dark" id="section_intro_3">
        <div class="">
            <div class="container">
                <div class="sixteen columns firas2">
                    <nav class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1" role="navigation">
                        <div class="containerss">
                            <div class="navbar-header">
                                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse-02">
                                    <span class="sr-only">Menu</span>
                                </button>
                                <img class="pix_nav_logo" alt="" src="images/main/logofelidays3.png">                
                            </div>
                            <div class="collapse navbar-collapse" id="navbar-collapse-02">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="active propClone"><a href="#">Inicio</a></li>
                                    <li class="propClone"><a href="#">Quienes Somos</a></li>
                                    <li class="propClone"><a href="#">Destinos</a></li>
                                    <li class="active propClone"><a href="#">Guias</a></li>
                                    <li class="propClone"><a href="#">Cotizador</a></li>
                                    <li class="propClone"><a href="#">Contacto</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container -->
                    </nav>
                </div>
            </div><!-- container -->
        </div>
        <!-- AQUI -->
        <!--<div id="themeSlider" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#themeSlider" data-slide-to="0" class="active"></li>
                <li data-target="#themeSlider" data-slide-to="1"></li>
            </ol>
        
            <div class="carousel-inner">
                <div class="item active">
                    <div class="imgOverlay"></div>
                    <img class="img-responsive" src="images/imagen1.jpg" alt="First slide">
                   
                    <div class="main-text hidden-xs hidden-sm">
                <div class="col-md-12 text-center">
                    <h1 class="titulo-blanco-bold">Lorem ipsum dolor sit amet</h1>
                    <h3 class="subtitulos-blanco">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                    <div class="clearfix"></div>
                    <div class="carousel-btns">
                        <a class="btn btn-md btn-default boton1" href="">Cotizame</a>
                    </div>
                </div>
            </div>
                </div>
                <div class="item">
                    <div class="imgOverlay"></div>
                    <img class="img-responsive" src="images/imagen3.jpg" alt="Second slide">
                    <div class="carousel-caption">
                        <h1 class="titulo-blanco-bold">Lorem ipsum dolor sit amet</h1>
                        <p class="subtitulos-blanco">Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </div>
                </div>
                
            </div>
        </div>-->
        <!-- Slider... -->
        <div class="col-lg-12 padding0">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <!--row_btn_slider-->
                <li id="slide_0" data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li id="slide_1" data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li id="slide_2" data-target="#carousel-example-generic" data-slide-to="2"></li>
                <!--row_btn_slider-->
              </ol>
              <div class="carousel-inner">
                <!--row_slider-->
                <div class="item active" id="div_carousel_0" role="listbox">
                    <!--Para grandes pantallas --> 
                    <div  class="g-bg-cover   g-bg-black-opacity-0_3--after tamano_slide" style="">
                        <img  src="images/imagen4.jpg" alt="" style="height:100%;" class=" img-fluid d-block g-bg-pos-top-center g-bg-img-hero">
                    </div> 
                    <!-- -->
                    <div class="header-text ">
                        <div id="" class="texto_slide container g-z-index-1" >
                          <div class="g-max-width-600">
                            <h2>
                                <span>Lorem ipsum dolor sit amet</span>
                            </h2>
                            <br>
                            <div class="carousel-btns">
                                <a class="btn btn-md btn-default boton1" href="">Cotizame</a>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="item" id="div_carousel_1">
                    <!--Para grandes pantallas --> 
                    <div  class="g-bg-cover   g-bg-black-opacity-0_3--after tamano_slide" style="">
                        <img  src="images/imagen2.jpg" alt="" style="height:100%;" class=" img-fluid d-block g-bg-pos-top-center g-bg-img-hero">
                    </div> 
                    <!-- -->
                    <div class="header-text hidden-xs hidden-sm">
                        <div id="" class="texto_slide container g-z-index-1" >
                            <div class="g-max-width-600">
                                <h2>
                                  <span>Lorem ipsum dolor sit amet</span>
                                </h2>
                                <br>
                            </div>
                        </div>
                   </div>
                </div>
                <div class="item" id="div_carousel_2">
                    <div  class=" g-bg-cover   g-bg-black-opacity-0_3--after tamano_slide" style="">
                        <img  src="images/imagen3.jpg" alt="" style="height:100%;" class=" img-fluid d-block g-bg-pos-top-center g-bg-img-hero">
                    </div> 
                    <div class="header-text hidden-xs hidden-sm">
                      <div id="" class="texto_slide container g-z-index-1" >
                          <div class="g-max-width-600">
                            <h2>
                              <span>Lorem ipsum dolor sit amet</span>
                            </h2>
                            <br>
                          </div>
                      </div>
                    </div>
                </div>
               <!--row_slider-->
              </div>
               <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- -->
        <!-- <div class="container">
            <div class="sixteen columns margin_bottom_50 padding_top_60">
                <div class="twelve offset-by-two columns">
                    <div class="center_text big_padding">
                        <p class="big_title bold_text editContent">Restaurant Landing Page</p>
                        <p class="big_text normal_gray editContent">
                            From logo design to website development, hand-picked designers and developers are ready to complete.
                        </p>
                        <a class="pix_button pix_button_line white_border_button bold_text big_text btn_big" href="#">
                            <i class="pi pixicon-paper"></i> 
                            <span>Make a Reservation</span>
                            </a>
                    </div>
                </div>
            </div>
            <div class="center_text">
                <a class="intro_arrow" href="#">
                    <i class="pi pixicon-arrow-down"></i> 
                </a>
            </div>
        </div> -->
</div>

    <!--AQUI EMPIEZA QUIENES SOMOS-->
    <div class="moto-widget moto-widget-row row-fixed" data-widget="row">
        <div class="container-fluid">
            <div class="moto-cell col-sm-12" data-container="container">
                <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                    <div class="moto-widget-spacer-block" style="height:30px"></div>
                </div>
                <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default wow fadeIn moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="maaa">
                    <div class="moto-widget-text-content moto-widget-text-editable">
                        <p style="text-align: center;" class="moto-text_system_3">Quienes Somos<br></p>
                    </div>
                </div>
                <div class="moto-widget moto-widget-row" data-widget="row">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="moto-cell col-sm-1" data-container="container"></div>
                            <div class="moto-cell col-sm-10" data-container="container">
                                <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                    <div class="moto-widget-spacer-block" style="height: 5px;"></div>
                                </div>
                                <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default                         wow fadeIn moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                        <p style="text-align: center;" class="moto-text_system_9">
                                            <span class="moto-color5_4">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                                            </span><br>
                                        </p>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-spacer moto-preset-default                                                moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="masa">
                                    <div class="moto-widget-spacer-block" style="height: 0px;"></div>
                                </div>
                            </div>
                            <div class="moto-cell col-sm-1" data-container="container"></div>
                        </div>
                    </div>
                </div>
                <div class="moto-widget moto-widget-row" data-widget="row">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="moto-cell col-sm-6" data-container="container">
                                <div class="moto-widget moto-widget-row" data-widget="row">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="moto-cell col-sm-9" data-container="container">
                                                <div data-animation="fadeInLeft" class="moto-widget moto-widget-text moto-preset-default                      wow fadeInLeft moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <p style="text-align: right;" class="moto-text_system_8">
                                                            <a class="moto-link" data-action="url" target="_self" data-cke-saved-href="#" href="#">MISSION</a><br>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div data-animation="fadeInLeft" class="moto-widget moto-widget-text moto-preset-default wow fadeInLeft moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <p style="text-align: right;" class="moto-text_normal">
                                                            Our clients are our greatest value. We are proud that our clients recommend us to their friends, relatives and acquaintances. We, in turn, are doing our utmost to make your rest positive and memorable, try to provide everything necessary to make you feel confident during.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="moto-cell col-sm-3" data-container="container">
                                                <div data-widget-id="wid__image__5b8743372b4e9" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto  wow fadeInLeft" data-widget="image">
                                                    <span class="moto-widget-image-link">
                                                        <img data-src="images/icon1.png" class="moto-widget-image-picture lazyload" data-id="150" title="" alt="">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                    <div class="moto-widget-spacer-block" style="height: 10px;"></div>
                                </div>
                            </div>
                            <div class="moto-cell col-sm-6" data-container="container">
                                <div class="moto-widget moto-widget-row" data-widget="row">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="moto-cell col-sm-3" data-container="container">
                                                <div data-widget-id="wid__image__5b8743372b77b" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto  wow fadeInRight" data-widget="image">
                                                    <span class="moto-widget-image-link">
                                                    <img data-src="images/icon2.png" class="moto-widget-image-picture lazyload" data-id="150" title="" alt="">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="moto-cell col-sm-9" data-container="container">
                                                <div data-animation="fadeInRight" class="moto-widget moto-widget-text moto-preset-default                      wow fadeInRight moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <p class="moto-text_system_8"><a class="moto-link" data-action="url" target="_self" data-cke-saved-href="#" href="#">AWARDS</a><br></p>
                                                    </div>
                                                </div>
                                                <div data-animation="fadeInRight" class="moto-widget moto-widget-text moto-preset-default                       wow fadeInRight moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <p class="moto-text_normal">During the work the company has received many awards: the absolute Favourite of Olympus in the nomination "The New Quality of Life"; received the award in the nomination "Bus Tours of the Year" at the international the contest "Choice of the Year".<br></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                    <div class="moto-widget-spacer-block" style="height: 10px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="moto-widget moto-widget-row" data-widget="row">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="moto-cell col-sm-6" data-container="container">
                                <div class="moto-widget moto-widget-row" data-widget="row">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="moto-cell col-sm-9" data-container="container">
                                                <div data-animation="fadeInLeft" class="moto-widget moto-widget-text moto-preset-default                      wow fadeInLeft moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <p style="text-align: right;" class="moto-text_system_8"><a class="moto-link" data-action="url" target="_self" data-cke-saved-href="#" href="#">PRINCIPLES</a><br></p>
                                                    </div>
                                                </div>
                                                <div data-animation="fadeInLeft" class="moto-widget moto-widget-text moto-preset-default                       wow fadeInLeft moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <p style="text-align: right;" class="moto-text_normal">Creation of each route is brought to the ideal. In creating tours and combination of routes experienced guides, responsible managers, and our CEO are involved. Groups of tourists are escorted by only experienced and erudite guides specializing in several countries.<br></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="moto-cell col-sm-3" data-container="container">
                                                <div data-widget-id="wid__image__5b8743372b9ee" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto  wow fadeInLeft" data-widget="image">
                                                    <span class="moto-widget-image-link">
                                                    <img data-src="images/icon3.png" class="moto-widget-image-picture lazyload" data-id="150" title="" alt="">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                    <div class="moto-widget-spacer-block" style="height: 10px;"></div>
                                </div>
                            </div>
                            <div class="moto-cell col-sm-6" data-container="container">
                                <div class="moto-widget moto-widget-row" data-widget="row">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="moto-cell col-sm-3" data-container="container">
                                                <div data-widget-id="wid__image__5b8743372bc50" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto  wow fadeInRight" data-widget="image">
                                                    <span class="moto-widget-image-link">
                                                        <img data-src="images/icon4.png" class="moto-widget-image-picture lazyload" data-id="150" title="" alt="">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="moto-cell col-sm-9" data-container="container">
                                                <div data-animation="fadeInRight" class="moto-widget moto-widget-text moto-preset-default                      wow fadeInRight moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <p class="moto-text_system_8"><a class="moto-link" data-action="url" target="_self" data-cke-saved-href="#" href="#">HISTORY</a><br></p>
                                                    </div>
                                                </div>
                                                <div data-animation="fadeInRight" class="moto-widget moto-widget-text moto-preset-default                       wow fadeInRight moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <p class="moto-text_normal">This year we celebrated 32 years. During this time we have developed many new tourism projects, created dozens of unique travel products and are not resting on our laurels. We have earned the trust of thousands of tourists who have traveled with us.<br></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                    <div class="moto-widget-spacer-block" style="height: 10px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="moto-widget moto-widget-spacer moto-preset-default                                                 moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="sama">
                    <div class="moto-widget-spacer-block" style="height: 5px;"></div>
                </div>
            </div>
        </div>
    </div>
                
    <!--FIN QUIENES SOMOS-->

    <div class="pixfort_hotel_5 " id="section_hotel_4_1">
            <div class="Homes pix_builder_bg fondo_gris">
               <div class="container">
                  <div class="sixteen columns">
                    <h1 class="title_homes editContent titulo-azul-bold">Destinos</h1>
                    <p class="subtitle_homes editContent c-gris1">
                        Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore.
                    </p>
                    <div class="row">
                        <!-- -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="thumbnail">
                                <img src="images/cards/1.jpg" alt="prueba" class="img-card">
                                <div class="caption">
                                    <h3 class="text-left c-naranja-oscuro">Roma</h3>
                                    <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                        <div class="col-lg-12 padding0">
                                            <span class="text-left c-naranja-oscuro">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star-o"></span>
                                            </span>
                                        </div>
                                        <div class="col-lg-12 padding0">
                                            <span class="texto-opiniones">
                                            430 opiniones
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right padding0">
                                        <span class="text-right texto-costo ">
                                            <span class="fa fa-eur" aria-hidden="true"></span>
                                            120,80
                                        </span>
                                    </div>
                                    <div class="col-lg-12 padding0 texto-cards">
                                        <span>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                        </span>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        </div>
                        <!-- -->
                        <!-- -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="thumbnail">
                                <img src="images/cards/2.jpg" alt="prueba" class="img-card">
                                <div class="caption">
                                    <h3 class="text-left c-naranja-oscuro">París</h3>
                                    <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                        <div class="col-lg-12 padding0">
                                            <span class="text-left c-naranja-oscuro">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                        </div>
                                        <div class="col-lg-12 padding0">
                                            <span class="texto-opiniones">
                                            210 opiniones
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right padding0">
                                        <span class="text-right texto-costo ">
                                            <span class="fa fa-eur" aria-hidden="true"></span>
                                            220,89
                                        </span>
                                    </div>
                                    <div class="col-lg-12 padding0 texto-cards">
                                        <span>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                        </span>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        </div>
                        <!-- -->
                        <!-- -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="thumbnail">
                                <img src="images/cards/3.jpg" alt="prueba" class="img-card">
                                <div class="caption">
                                    <h3 class="text-left c-naranja-oscuro">Manchester</h3>
                                    <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                        <div class="col-lg-12 padding0">
                                            <span class="text-left c-naranja-oscuro">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                            </span>
                                        </div>
                                        <div class="col-lg-12 padding0">
                                            <span class="texto-opiniones">
                                            40 opiniones
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right padding0">
                                        <span class="text-right texto-costo ">
                                            <span class="fa fa-eur" aria-hidden="true"></span>
                                            420,00
                                        </span>
                                    </div>
                                    <div class="col-lg-12 padding0 texto-cards">
                                        <span>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                        </span>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        </div>
                        <!-- -->
                        <!-- -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="thumbnail">
                                <img src="images/cards/4.jpg" alt="prueba" class="img-card">
                                <div class="caption">
                                    <h3 class="text-left c-naranja-oscuro">Londres</h3>
                                    <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                        <div class="col-lg-12 padding0">
                                            <span class="text-left c-naranja-oscuro">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                            </span>
                                        </div>
                                        <div class="col-lg-12 padding0">
                                            <span class="texto-opiniones">
                                            130 opiniones
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right padding0">
                                        <span class="text-right texto-costo ">
                                            <span class="fa fa-eur" aria-hidden="true"></span>
                                            190,90
                                        </span>
                                    </div>
                                    <div class="col-lg-12 padding0 texto-cards">
                                        <span>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                        </span>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        </div>
                        <!-- -->
                        <!-- -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="thumbnail">
                                <img src="images/cards/5.jpg" alt="prueba" class="img-card">
                                <div class="caption">
                                    <h3 class="text-left c-naranja-oscuro">Madrid</h3>
                                    <div class="col-md-6 text-left p-left-0 ">
                                        <div class="col-lg-12 padding0">
                                            <span class="text-left c-naranja-oscuro">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                            </span>
                                        </div>
                                        <div class="col-lg-12 padding0">
                                            <span class="texto-opiniones">
                                            89 opiniones
                                            </span>
                                        </div>    
                                    </div>
                                    <div class="col-md-6 text-right padding0">
                                        <span class="text-right texto-costo ">
                                            <span class="fa fa-eur" aria-hidden="true"></span>
                                            105,90
                                        </span>
                                    </div>
                                    <div class="col-lg-12 padding0 texto-cards">
                                        <span>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                        </span>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        </div>
                        <!-- -->
                        <!-- -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="thumbnail">
                                <img src="images/cards/6.jpg" alt="prueba" class="img-card">
                                <div class="caption">
                                    <h3 class="text-left c-naranja-oscuro">New York</h3>
                                    <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                        <div class="col-lg-12 padding0">
                                            <span class="text-left c-naranja-oscuro">
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                                <span class="fa fa-star-o"></span>
                                            </span>
                                        </div>
                                        <div class="col-lg-12 padding0">
                                            <span class="texto-opiniones">
                                            50 opiniones
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right padding0">
                                        <span class="text-right texto-costo ">
                                            <span class="fa fa-eur" aria-hidden="true"></span>
                                            14,40
                                        </span>
                                    </div>
                                    <div class="col-lg-12 padding0 texto-cards">
                                        <span>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                        </span>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        </div>
                        <!-- -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dark big_padding pix_builder_bg" id="section_title_2_dark">
		<div class="container ">       
	            <div class="twelve columns offset-by-two">
	            	<div class="center_text">
	            		<div class="title_56 margin_bottom_10 dark_yellow">
				            <span class="pi pixicon-browser"></span>
				        </div>
	            		<h2 class="big_title dark_gray editContent">Dark Heading Style</h2>
	                	<p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad  voluptate velit esse cillum dolore eu fugiat.</p>
	                </div>
	            </div>
	    </div>
	</div><div class="center_text big_padding pix_builder_bg" id="section_intro_title">
            <div class="container ">       
                    <div class="one-third column">
                        <div class="simple_team">
                            <div class="simple_team_img ">
                                <img alt="" src="images/main/placeholder150.png">
                            </div>
                            <div>
                                <h4 class="editContent gray">
                                    <strong>John Doe</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <ul class="bottom-icons">
                                    <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="one-third column">
                        <div class="simple_team">
                            <div class="simple_team_img ">
                                <img alt="" src="images/main/placeholder150.png">
                            </div>
                            <div>
                                <h4 class="editContent gray">
                                    <strong>Marc Smith</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <ul class="bottom-icons">
                                    <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="one-third column">
                        <div class="simple_team">
                            <div class="simple_team_img ">
                                <img alt="" src="images/main/placeholder150.png">
                            </div>
                            <div>
                                <h4 class="editContent gray">
                                    <strong>Sarah Stone</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <ul class="bottom-icons">
                                    <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="one-third column">
                        <div class="simple_team">
                            <div class="simple_team_img ">
                                <img alt="" src="images/main/placeholder150.png">
                            </div>
                            <div>
                                <h4 class="editContent gray">
                                    <strong>John Doe</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <ul class="bottom-icons">
                                    <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="one-third column">
                        <div class="simple_team">
                            <div class="simple_team_img ">
                                <img alt="" src="images/main/placeholder150.png">
                            </div>
                            <div>
                                <h4 class="editContent gray">
                                    <strong>Marc Smith</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <ul class="bottom-icons">
                                    <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="one-third column">
                        <div class="simple_team">
                            <div class="simple_team_img ">
                                <img alt="" src="images/main/placeholder150.png">
                            </div>
                            <div>
                                <h4 class="editContent gray">
                                    <strong>Sarah Stone</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <ul class="bottom-icons">
                                    <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
            </div>
        </div><div class="pix_builder_bg" id="section_call_2">
        <div class="footer3">
            <div class="container ">
                
                    <div class="content_div center_text">
                        <a class="pix_button pix_button_line bold_text green_1 btn_big" href="#">
                            <span>CALL TO ACTION BUTTON</span>
                        </a>    
                    </div>
  
            </div>
        </div>
    </div>
    <div id="themeSlider2" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#themeSlider2" data-slide-to="0" class="active"></li>
            <li data-target="#themeSlider2" data-slide-to="1"></li>
            <li data-target="#themeSlider2" data-slide-to="2"></li>
        </ol>
    
        <div class="carousel-inner">
            <div class="item active">
                <div class="big_padding pix_builder_bg fondo_gris " id="section_normal_4_1">
                    <div class="container ">
                        <div class="sixteen columns">
                            <div class="eight columns alpha margin_bottom_30">
                                    <span class="bold_text big_text blue_text editContent">GET FREE SUPPORT</span>
                                    <p class="big_title texto_negro bold_text editContent">Get The Most Amazing Builder!</p>
                                    <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                    <a class="pix_button btn_normal pix_button_flat blue_bg bold_text" href="#">
                                        <i class="pi pixicon-download"></i> 
                                        <span>GET SOFTWARE</span>
                                    </a> 
            
                                </div>
                            <div class="eight columns omega pix_container">
                            <?php include('rotativo.html') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="big_padding pix_builder_bg dark" id="section_normal_4_1">
                    <div class="container">
                        <div class="sixteen columns">
                            <div class="eight columns alpha margin_bottom_30">
                                    <span class="bold_text big_text blue_text editContent">GET FREE SUPPORT</span>
                                    <p class="big_title bold_text editContent">Get The Most Amazing Builder!</p>
                                    <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                    <a class="pix_button btn_normal pix_button_flat blue_bg bold_text" href="#">
                                        <i class="pi pixicon-download"></i> 
                                        <span>GET SOFTWARE</span>
                                    </a> 
            
                                </div>
                            <div class="eight columns omega pix_container">
                                <img class="img_style" alt="" src="images/cards/roma1.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="big_padding pix_builder_bg dark" id="section_normal_4_1">
                    <div class="container">
                        <div class="sixteen columns">
                            <div class="eight columns alpha margin_bottom_30">
                                    <span class="bold_text big_text blue_text editContent">GET FREE SUPPORT</span>
                                    <p class="big_title bold_text editContent">Get The Most Amazing Builder!</p>
                                    <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                    <a class="pix_button btn_normal pix_button_flat blue_bg bold_text" href="#">
                                        <i class="pi pixicon-download"></i> 
                                        <span>GET SOFTWARE</span>
                                    </a> 
            
                                </div>
                            <div class="eight columns omega pix_container">
                                <img class="img_style" alt="" src="images/cards/loros.png">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
                
        
    </div>
    <!-- <div class="big_padding pix_builder_bg dark" id="section_normal_4_1">
        <div class="container">
            <div class="sixteen columns">
                <div class="eight columns alpha margin_bottom_30">
                        <span class="bold_text big_text blue_text editContent">GET FREE SUPPORT</span>
                        <p class="big_title bold_text editContent">Get The Most Amazing Builder!</p>
                        <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        <a class="pix_button btn_normal pix_button_flat blue_bg bold_text" href="#">
                            <i class="pi pixicon-download"></i> 
                            <span>GET SOFTWARE</span>
                        </a> 

                    </div>
                <div class="eight columns omega pix_container">
                    <img class="img_style" alt="" src="images/main/software-img-3.png">
                </div>
            </div>
        </div>
    </div> -->
    <div class="pixfort_text_4 pix_builder_bg" id="section_text_4">
        <div class="footer3">
            <div class="container ">
                <div class="five columns alpha">
                	<div class="content_div area_1">
                        <img class="pix_footer_logo" alt="" src="images/main/footer-logo-3.png">                
                    	<p class="small_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt.</p>
                        <ul class="bottom-icons">
                            <li><a class="pi pixicon-facebook2" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-twitter2" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-instagram" href="#fakelink"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="three columns">
    				<div class="content_div area_2">
    					<span class="pix_text"><span class="editContent footer3_title">Navigation</span></span>
                    	<ul class="footer3_menu">
                            <li><a class="pix_text" href="#"><span class="editContent">Home</span></a></li>
                            <li><a class="pix_text" href="#"><span class="editContent">Overview</span></a></li>
                            <li><a class="pix_text" href="#"><span class="editContent">About</span></a></li>
                            <li><a class="pix_text" href="#"><span class="editContent">Buy now</span></a></li>
                            <li><a class="pix_text" href="#"><span class="editContent">support</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="four columns">
    				<div class="content_div area_3">
    					<span class="pix_text"><span class="editContent big_number">347 567 78 90</span></span>
                        <span class="pix_text"><span class="editContent small_bold light_color">AVAILABLE FROM 12PM - 18PM</span></span>
                        <h4 class="editContent med_title">New York, NY</h4>
                    	<p class="editContent small_bold">560 Judah St &amp; 15th Ave, Apt 5 San Francisco, CA, 230903</p>
                    </div>
                </div>
                <div class="four columns omega">
                	<div class="content_div">
                		<span class="pix_text"><span class="editContent footer3_title">Info</span></span>
                    	<p class="editContent ">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt Lorem ipsum dolor sit amet consectetur adipiscing elit sed.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pixfort_real_estate_4" id="section_real_estate_9">
		<div class="foot_st pix_builder_bg">
			<div class="container ">
				<div class="seven columns alpha ">
					<span class="editContent"><span class="pix_text"><span class="rights_st"> All rights reserved Copyright © 2014 FLATPACK by <span class="pixfort_st">PixFort</span>
				</span></span></span>
			</div>
			<div class="nine columns omega ">
				<div class="socbuttons">
					<div class="soc_icons pix_builder_bg">
						<ul class="bottom-icons">
                            <li><a class="pi pixicon-facebook2 normal_gray" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-twitter2 normal_gray" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-instagram normal_gray" href="#fakelink"></a></li>
                        </ul>
					</div>
					<div class="likes_st">
						<span class="editContent"><span class="pix_text">Your likes &amp; share makes us happy!</span></span>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<!-- JavaScript
================================================== -->
<!-- <script src="js-files/jquery-1.8.3.min.js" type="text/javascript"></script> -->
<script src="assets/js/website.assets.min7c7f.js?_build=1534856457" type="text/javascript" data-cfasync="false"></script>
<script type="text/javascript" data-cfasync="false">
    var websiteConfig = websiteConfig || {};
    websiteConfig.address = 'index.html';
    websiteConfig.addressHash = '97852ff5a70d64b573167d0c0e10f555';
    websiteConfig.apiUrl = 'api.json';
    websiteConfig.preferredLocale = 'en_US';
    websiteConfig.preferredLanguage = websiteConfig.preferredLocale.substring(0, 2);
            websiteConfig.back_to_top_button = {"topOffset":300,"animationTime":500,"type":"theme"};
            websiteConfig.popup_preferences = {"loading_error_message":"The content could not be loaded."};
    websiteConfig.lazy_loading = {"enabled":true};
    websiteConfig.cookie_notification = {"enabled":false,"content":"<p class=\"moto-text_normal\">This website uses cookies to ensure you get the best experience on our website.<\/p>","content_hash":"6200d081e0d72ab7f5a8fe78d961c3ec"};
    angular.module('website.plugins', []);
</script>
<script src="assets/js/website.min32ab.js?_build=1534856445" type="text/javascript" data-cfasync="false"></script>

<script src="js-files/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="js-files/jquery.common.min.js" type="text/javascript"></script>
<script src="js-files/ticker.js" type="text/javascript"></script>
<script src="js-files/custom1.js" type="text/javascript"></script>
<script src="assets/js/smoothscroll.min.js" type="text/javascript"></script>
<script src="assets/js/appear.min.js" type="text/javascript"></script>
<script src="js-files/jquery.ui.touch-punch.min.js"></script>
<!-- <script src="js-files/bootstrap.min.js"></script> -->
<script src="js-files/bootstrap-switch.js"></script>
<script src="js-files/custom3.js" type="text/javascript"></script>


<script src="assets/js/appear.min.js" type="text/javascript"></script>
<script src="assets/js/animations.js" type="text/javascript"></script>
<script src="assets/js/fbasic.js" type="text/javascript"></script>  

</body>
</html>
