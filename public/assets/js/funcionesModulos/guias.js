$(document).ready(function(){
	$(document).on("click","#btn_ver_mas",function(){
		var valores = $("#contenedor-guias").attr("data");
		var vector = valores.split("|");
		var offset = vector[0];
		var limit = vector[1];
		var token = $("inpute:text[name=_token]").val();
		data = {
					"_token":token,
					"offset": offset,
					"limit": limit,
					"idioma": 1,

		}
		$.ajax({
				url:"/verMasGuias",
					type:'POST',
					cache:false,
					data:data,
				headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    },
					error: function(resp){
						console.log(resp);
					},
					success: function(resp){
						//var recordset = $.parseJSON(resp);
						if(resp!="No hay datos para esta consulta"){
							$("#contenedor-guias").append(resp).hide().fadeIn(2000);
							offset = parseInt(offset)+1; 
							data=$("#contenedor-guias").attr("data",offset+"|"+limit)
						}else{
							mensaje_alerta("#cuerpoMensaje","Ya han sido cargados los guías disponibles!", "success")
						}
					}
		});
	});
});