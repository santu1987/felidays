function cambiarIdioma(sel, url) {
    if (sel.value == "es") {
        window.location.href = "/felidays/public/" + sel.value;
        $("#selectIdiomas").val("es");
    }
    if (sel.value == "en") {
        window.location.href = "/felidays/public/" + sel.value;
        $("#selectIdiomas").val("en");
    }
}

$("#selectIdiomas").val($("#idiomaSeleccionado").val())