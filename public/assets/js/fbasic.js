$( document ).ready(function() {
	//$('#carouselExampleIndicators').carousel({interval: 6000});
	//$("#div_carousel_1,#slide_1").addClass("active");
	//console.log("aqui");
})

//Mensaje de validacion
function mensaje_alerta(campo, mensaje, tipo_alerta)
{
  $(campo).removeClass("alert alert-danger").removeClass("alert alert-success").removeClass("alert alert-info").removeClass("alert alert-primary");
  $(campo).addClass("alert alert-"+tipo_alerta);
  $(campo).html(mensaje);
  setTimeout(function()
  {
    $(campo).fadeOut(1400);
  },10000);
  $(campo).show();
}