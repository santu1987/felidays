
function rango(myRange,demo){
	var slider = document.getElementById(myRange);
	var output = document.getElementById(demo);
	output.innerHTML = slider.value;

	slider.oninput = function() {
	  output.innerHTML = this.value;
	}
}

rango("myRange1","demo1");
rango("myRange2","demo2");