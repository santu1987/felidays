<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeccionesParallax extends Model
{
    protected $table = 'seccionesparallax';
    protected $fillable = [];
    public $timestamps = false;
}
