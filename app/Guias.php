<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guias extends Model
{
    protected $table = 'guias';
    protected $fillable = [];
    public $timestamps = false;
}
