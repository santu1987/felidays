<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idiomas extends Model
{
    protected $table = 'Idiomas';
    protected $fillable = [];
    public $timestamps = false;
}
