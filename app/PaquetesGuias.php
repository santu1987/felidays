<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaquetesGuias extends Model
{
    protected $table = 'PaquetesGuias';
    protected $fillable = [];
    public $timestamps = false;
}
