<?php

namespace App\Http\Controllers\WEB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuiasController extends Controller
{
    public function getIndex($Idioma){
        $idIdioma=1;
        if($Idioma=="en"){
            $idIdioma=2;
        }
        $offset = 0;
        $limit = 1;
       	$guias = \App\Helpers\Rest::consumirRest('/API/consultarGuias/'.$idIdioma.'/'.$limit.'/'.$offset,'GET');
        $parallax = \App\Helpers\Rest::consumirRest('/API/consultarParallax/'.$idIdioma.'/guias','GET');
        $menu = \App\Helpers\Rest::consumirRest('/API/consultarRutas/'.$idIdioma,'GET');
        $offset2 = $offset+1;
        $filtros = array("idioma"=>$Idioma,"offset"=>$offset2,"limit"=>$limit);
        return view('guias.index',compact('guias','parallax','filtros','menu','Idioma'));
    }

    public function verMas(Request $request){
        $idIdioma =$request->input("idioma");
        $offset = $request->input("offset");
        $limit = $request->input("limit");

        $guias = \App\Helpers\Rest::consumirRest('/API/consultarGuias/'.$idIdioma.'/'.$limit.'/'.$offset,'GET');
        $parallax = \App\Helpers\Rest::consumirRest('/API/consultarParallax/'.$idIdioma.'/guias','GET');
        if($guias!="No hay datos para esta consulta"){
            $menu = \App\Helpers\Rest::consumirRest('/API/consultarRutas/'.$idIdioma,'GET');
            $filtros = array("idioma"=>$idIdioma,"offset"=>$offset,"limit"=>$limit);
            return view('guias.masGuias',compact('guias','parallax','filtros','menu','Idioma'));    
        }else{
            return $guias;
        }
       
    }

    public function verGuiasDetalle($Idioma,$id){
        $idIdioma=1;
        if($Idioma=="en"){
            $idIdioma=2;
        }
        
        $guias = \App\Helpers\Rest::consumirRest('/API/consultarGuiasDetalle/'.$idIdioma.'/'.$id,'GET');
        
        $parallax = \App\Helpers\Rest::consumirRest('/API/consultarParallax/'.$idIdioma.'/guias','GET');

        $menu = \App\Helpers\Rest::consumirRest('/API/consultarRutas/'.$idIdioma,'GET');

        return view('guias.detallesGuias',compact('guias','parallax','menu','Idioma'));    
    }
}
