<?php

namespace App\Http\Controllers\WEB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ContactosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex($Idioma)
    {
        $idIdioma = 1;
        if ($Idioma == "en") {
            $idIdioma = 2;
        }
        $menu = \App\Helpers\Rest::consumirRest('/API/consultarRutas/' . $idIdioma, 'GET');
        $parallax = \App\Helpers\Rest::consumirRest('/API/consultarParallax/' . $idIdioma . '/contacto', 'GET');
        $paises = \App\Helpers\Rest::consumirRest('/API/consultarPaises/' . $idIdioma, 'GET');
        return view('contactos.index', compact('menu', 'Idioma', 'parallax', 'paises'));
    }
}
