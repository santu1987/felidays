<?php

namespace App\Http\Controllers\WEB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuienesSomosController extends Controller
{
    public function getIndex($Idioma){
        $idIdioma=1;
        if($Idioma=="en"){
            $idIdioma=2;
        }
        $qsomos = \App\Helpers\Rest::consumirRest('/API/consultarEmpresas/'.$idIdioma,'GET');
        $parallax = \App\Helpers\Rest::consumirRest('/API/consultarParallax/'.$idIdioma.'/qsomos','GET');
        $equipo =\App\Helpers\Rest::consumirRest('/API/consultarEquipo/'.$idIdioma,'GET');
        $menu = \App\Helpers\Rest::consumirRest('/API/consultarRutas/'.$idIdioma,'GET');
        return view('qsomos.index',compact('qsomos','parallax','equipo','menu','Idioma'));
    }
}
