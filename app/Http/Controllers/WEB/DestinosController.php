<?php

namespace App\Http\Controllers\WEB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DestinosController extends Controller
{
    public function getIndex($Idioma){
        $idIdioma=1;
        if($Idioma=="en"){
            $idIdioma=2;
        }
        return view('destinos.index');
    }
    public function getDestinoCiudadPais($idCiudad,$idPais,$Idioma){
    	$idIdioma=1;
        if($Idioma=="en"){
            $idIdioma=2;
        }
    	//--Aqui debo consultar todos los paquetes asociados a ese pais y esa ciudad
    	//--
        $ciudad = \App\Helpers\Rest::consumirRest('/API/consultarCiudad/'.$idCiudad,'GET');
    	$menu = \App\Helpers\Rest::consumirRest('/API/consultarRutas/'.$idIdioma,'GET');
    	return view('destinos.paquetesPaisCiudad',compact('ciudad','menu','Idioma'));
    }
    /*
    *
    */
    public function getDestino($Idioma){
        $idIdioma=1;
        if($Idioma=="en"){
            $idIdioma=2;
        }
        //--Aqui debo consultar todos los paquetes asociados a ese pais y esa ciudad
        //--
        $idCiudad=1;
        $ciudad = \App\Helpers\Rest::consumirRest('/API/consultarCiudad/'.$idCiudad,'GET');
        $menu = \App\Helpers\Rest::consumirRest('/API/consultarRutas/'.$idIdioma,'GET');
        return view('destinos.paquetesDestinos',compact('ciudad','menu','Idioma'));
    }
    /*
    *
    */
}
