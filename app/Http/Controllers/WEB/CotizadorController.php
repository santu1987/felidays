<?php

namespace App\Http\Controllers\WEB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CotizadorController extends Controller
{

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex($Idioma){
        $idIdioma=1;
        if($Idioma=="en"){
            $idIdioma=2;
        }
    	return view('cotizador.index');
    }
}
