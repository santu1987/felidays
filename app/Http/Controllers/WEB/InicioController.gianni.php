<?php

namespace App\Http\Controllers\WEB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InicioController extends Controller
{
    public function index($Idioma){
        $idIdioma=1;
        if($Idioma=="en"){
            $idIdioma=2;
        }
        //Asignando limites a guias
        $limit = 6;
        $offset = 0;
        //Consumiendo apis internas
        $sliders = \App\Helpers\Rest::consumirRest('/API/consultarSlider/'.$idIdioma,'GET');
        $guias = \App\Helpers\Rest::consumirRest('/API/consultarGuias/'.$idIdioma.'/'.$limit.'/'.$offset,'GET');
        $parallaxGuias = \App\Helpers\Rest::consumirRest('API/consultarParallaxGuias/'.$idIdioma,'GET');
        //$paquetes = \App\Helpers\Rest::consumirRest('/API/consultarPaquetes/'.$idIdioma,'GET');
        //$qsomos = \App\Helpers\Rest::consumirRest('/API/consultarEmpresas/'.$idIdioma,'GET');
        return view('inicio.index', compact('sliders','guias','parallaxGuias'));
    }
}
