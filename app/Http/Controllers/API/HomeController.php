<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Exception;
use App\Sliders;
use App\SeccionesParallax;
use App\Footers;

class HomeController extends Controller
{
    /*
	*	SLIDER POR IDIOMA
    */
    public function getSlider($idIdioma){
        try{
            if($slider=Sliders::where('idIdioma','=',$idIdioma)->get()){
            	if(count($slider) == 0){
            		$respuesta = "[".json_encode(array('idSlider'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
            	}else{
                    foreach($slider as $valor){
                        $valor["boton"] = json_decode($valor["boton"]);
                        $sliders[]=$valor;
                    }
            		$respuesta = $sliders;
            	}
            }
        }
        catch(Exception $e){
        	$respuesta = "[".json_encode(array('idSlider'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
        	return $respuesta;
        }
    }
    /*
	*	PARALLAX POR IDIOMA
    */
    public function getParallax($idIdioma){

        try{
            if($parallax=SeccionesParallax::where('idIdioma','=',$idIdioma)->get()){
            	if(count($parallax) == 0){
            		$respuesta = "[".json_encode(array('idSeccionP'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
            	}else{
            		$respuesta = $parallax;
            	}
            }
        }
        catch(Exception $e){
        	$respuesta = "[".json_encode(array('idSeccionP'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
        	return $respuesta;
        }
    }
    /*
	*	FOOTTER POR IDIOMA
    */
    public function getFooter($idIdioma){
        try{
            if($footer=Footers::where('idIdioma','=',$idIdioma)->get()){
            	if(count($footer) == 0){
            		$respuesta = "[".json_encode(array('idFooter'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
            	}else{
            		$respuesta = $footer;
            	}
            }
        }
        catch(Exception $e){
        	$respuesta = "[".json_encode(array('idFooter'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
        	return $respuesta;
        }
    }
}
