<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Equipo;

class EquipoController extends Controller
{
    public function getEquipo($idIdioma){
        try{
            if($Equipo=Equipo::where('idIdioma','=',$idIdioma)->get())
            {
                if(count($Equipo) == 0){
                    $respuesta="[".json_encode(array('idEquipo'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
                }
                else{
                    $respuesta=$Equipo;
                }                
            }
        }
        catch(Exception $e){
            $respuesta="[".json_encode(array('idEquipo'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
            return $respuesta;
        }
    }
}
