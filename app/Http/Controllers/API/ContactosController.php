<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contactos;
use App\Formularios;
use App\Paises;
use App\Idiomas;
use App\SeccionesParallax;

class ContactosController extends Controller
{
    //traer los datos de la pagina de contactos
    public function getContactos($idIdioma)
    {
        try {
            if ($contacto = Contactos::where('idIdioma', '=', $idIdioma)->get()) {
                if (count($contacto) == 0) {
                    $respuesta = "[" . json_encode(array('idcontacto' => 0, 'mensaje' => 'No hay datos para esta consulta')) . "]";
                } else {
                    //$banner=SeccionesParallax::where('idIdioma','=',$idIdioma)->where('seccion','=','contacto')->get();
                    //concateno las dos consultas y quito los corchetes
                    $respuesta = $contacto;
                }
            }
        } catch (Exception $e) {
            $respuesta = "[" . json_encode(array('idContacto' => 0, 'mensaje' => 'Error')) . "]";
        }
        finally {
            return $respuesta;
        }
    }  
    
    //registros desde los formularios de contacto
    public function postFormulario(Request $request)
    {
        print_r($request);
        die();
        $respuesta = "";
        try {
            $idIdioma = $request->input('idIdioma');
            $idPais = $request->input('selectPais');
            $nombre = $request->input('txtNombre') . " " . $request->input('txtApellido');
            $email = $request->input('txtEmail');
            $telefono = $request->input('txtTelefono');
            $tipo = $request->input('asunto');
            $documento = $request->input('txtDocumento');
            $direccion = $request->input('txtDireccion');
            $ciudad = $request->input('txtCiudad');
            $mensaje = $request->input('txtMensaje');

            $formulario = new Formularios;
            $formulario->idIdioma = $idIdioma;
            $formulario->idPais = $idPais;
            $formulario->nombre = $nombre;
            $formulario->email = $email;
            $formulario->telefono = $telefono;
            $formulario->tipo = $tipo;
            $formulario->documentoIdentidad = $documento;
            $formulario->direccion = $direccion;
            $formulario->ciudad = $ciudad;
            $formulario->mensaje = $mensaje;
            if ($formulario->save()) {
                //Enviar correo
                //Mail::to('ventas.web@incable.com')->send(new MailCotizacion($cotizacion->id, $email, $nombre, $telefono, $msg, $ps, $pais, $ciudad, $empresa));              

                $respuesta = "[" . json_encode(array('idFormulario' => $formulario->id, 'mensaje' => 'Exito')) . "]";

            } else {
                $respuesta = "[" . json_encode(array('idFormulario' => 0, 'mensaje' => 'Error')) . "]";
            }
            return $respuesta;
        } catch (Exception $e) {
            $respuesta = "[" . json_encode(array('idFormulario' => 0, 'mensaje' => 'Error')) . "]";
            return $respuesta;
        }

    }
}
