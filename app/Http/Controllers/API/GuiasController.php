<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Exception;
use App\Guias;
use App\SeccionesParallax;

class GuiasController extends Controller
{
    /*
	*	GUIAS POR IDIOMA
    */
    public function getGuias($idIdioma,$limit,$offset){
        $limit = (integer)$limit;
        $offset = (integer)$offset;
          
        try{
            if($guias=Guias::join('ciudades','guias.idCiudad','=','ciudades.idCiudad')->join('paises','paises.idPais','=','ciudades.idPais')->where('guias.idIdioma','=',$idIdioma)->offset($offset)->limit($limit)->get(['guias.*','ciudades.nombre as nombreCiudad','paises.bandera as bandera'])){
            	if(count($guias) == 0){
            		$respuesta = "[".json_encode(array('idGuia'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
            	}else{
                    foreach ($guias as $valor) {
                        $valoracion = 0;
                        $res_evaluacion = $this->getGuiasValoracion($valor["idGuia"]);
                        $n = 0;
                        foreach ($res_evaluacion as $valor2) {
                            $valoracion = $valoracion + (integer)$valor2["valor"];
                            $n++;
                        }
                        $valor["evaluacion"] = round($valoracion / $n);
                        
                        if($valor["idIdioma"]==1){
                            $valor["idioma"] = "es";
                        }else{
                            $valor["idioma"] = "en";
                        }                       
                        
                        $listado_guias[] = $valor;
                    }
            		$respuesta = $listado_guias;
            	}
            }
        }
        catch(Exception $e){
        	$respuesta = "[".json_encode(array('idGuia'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
        	return $respuesta;
        }
    }
    /*
    *   PARALLAX POR IDIOMA
    */
    public function getParallax($idIdioma){
       
        try{
            if($parallax=SeccionesParallax::where('idIdioma','=',$idIdioma)->where('seccion','=','guias')->get()){
                if(count($parallax) == 0){
                    $respuesta = "[".json_encode(array('idSeccionP'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
                }else{
                    $respuesta = $parallax;
                }
            }
        }
        catch(Exception $e){
            $respuesta = "[".json_encode(array('idSeccionP'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
            return $respuesta;
        }
    }
    /*
    *  VALORACION POR GUIA
    */
    public function getGuiasValoracion($idGuia){
        try{
            if($evaluaciones=Guias::join('evaluaciones','evaluaciones.idGuia','=','guias.idGuia')->where('guias.idGuia','=',$idGuia)->get(['valor'])){
                if(count($evaluaciones) == 0){
                    $respuesta = "[".json_encode(array('idGuia'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
                }else{
                    $respuesta = $evaluaciones;
                }
            }
        }
        catch(Exception $e){
            $respuesta = "[".json_encode(array('idGuia'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
            return $respuesta;
        }
    }
    /*
    *   GUIAS DETALLES
    */
    public function consultarGuiasDetalle($idIdioma,$idGuia){
         try{
            if($guias=Guias::join('ciudades','guias.idCiudad','=','ciudades.idCiudad')->join('paises','paises.idPais','=','ciudades.idPais')->where('guias.idIdioma','=',$idIdioma)->where('guias.idGuia','=',$idGuia)->get(['guias.*','ciudades.nombre as nombreCiudad','paises.bandera as bandera'])){
                if(count($guias) == 0){
                    $respuesta = "[".json_encode(array('idGuia'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
                }else{
                    foreach ($guias as $valor) {
                        $valoracion = 0;
                        $res_evaluacion = $this->getGuiasValoracion($valor["idGuia"]);
                        $n = 0;
                        
                        foreach ($res_evaluacion as $valor2) {
                            $valoracion = $valoracion + (integer)$valor2["valor"];
                            $n++;
                        }
                        
                        $valor["evaluacion"] = round($valoracion / $n);
                        
                        if($valor["idIdioma"]==1){
                            $valor["idioma"] = "es";
                        }else{
                            $valor["idioma"] = "en";
                        }  

                        $listado_guias[] = $valor;
                    }
                    $respuesta = $listado_guias;
                }
            }
        }
        catch(Exception $e){
            $respuesta = "[".json_encode(array('idGuia'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
            return $respuesta;
        }
    }
}    