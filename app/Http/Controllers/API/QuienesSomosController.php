<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Exception;
use App\Empresas;
use App\SeccionesParallax;

class QuienesSomosController extends Controller
{
    public function getEmpresas($idIdioma){
        try{
            
            if($empresa=Empresas::join('rutas', 'empresas.idIdioma', '=', 'rutas.idIdioma')->where('empresas.idIdioma','=',$idIdioma)->where('rutas.seccion','=','qSomos')->get())
            {
                if(count($empresa) == 0){
                    $respuesta="[".json_encode(array('idEmpresa'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
                }
                else{
                    $respuesta=$empresa;                
                }                
            }
        }
        catch(Exception $e){
            $respuesta="[".json_encode(array('idEmpresa'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
            return $respuesta;
        }
    }
}
