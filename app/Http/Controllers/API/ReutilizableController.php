<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Paises;
use App\Idiomas;
use App\Footers;
use App\Ciudades;
use App\SeccionesParallax;
use App\Rutas;

class ReutilizableController extends Controller
{
    //Trae todos los idiomas registrados
    public function getIdiomas()
    {
        try {
            if ($idioma = Idiomas::all()) {
                if (count($idioma) == 0) {
                    $respuesta = "[" . json_encode(array('idIdioma' => 0, 'mensaje' => 'No hay datos para esta consulta')) . "]";
                } else {
                    $respuesta = $idioma;
                }
            }
        } catch (Exception $e) {
            $respuesta = "[" . json_encode(array('idIdioma' => 0, 'mensaje' => 'Error')) . "]";
        }
        finally {
            return $respuesta;
        }
    }

    //Trae todos los paises
    public function getPaises($idIdioma)
    {
        try {
            if ($pais = Paises::where('idIdioma', '=', $idIdioma)->get()) {
                if (count($pais) == 0) {
                    $respuesta = "[" . json_encode(array('idPais' => 0, 'mensaje' => 'No hay datos para esta consulta')) . "]";
                } else {
                    $respuesta = $pais;
                }
            }
        } catch (Exception $e) {
            $respuesta = "[" . json_encode(array('idPais' => 0, 'mensaje' => 'Error')) . "]";
        }
        finally {
            return $respuesta;
        }
    }

    /*
     *	FOOTTER POR IDIOMA
     */
    public function getFooter($idIdioma)
    {
        try {
            if ($footer = Footers::where('idIdioma', '=', $idIdioma)->get()) {
                if (count($footer) == 0) {
                    $respuesta = "[" . json_encode(array('idFooter' => 0, 'mensaje' => 'No hay datos para esta consulta')) . "]";
                } else {
                    $respuesta = $footer;
                }
            }
        } catch (Exception $e) {
            $respuesta = "[" . json_encode(array('idFooter' => 0, 'mensaje' => 'Error')) . "]";
        }
        finally {
            return $respuesta;
        }
    }

    //Trae todas las ciudades
    public function getCiudades()
    {
        try {
            if ($ciudad = Ciudades::all()) {
                if (count($ciudad) == 0) {
                    $respuesta = "[" . json_encode(array('idCiudad' => 0, 'mensaje' => 'No hay datos para esta consulta')) . "]";
                } else {
                    $respuesta = $ciudad;
                }
            }
        } catch (Exception $e) {
            $respuesta = "[" . json_encode(array('idCiudad' => 0, 'mensaje' => 'Error')) . "]";
        }
        finally {
            return $respuesta;
        }
    }
    //Trae todas las ciudades por pais
    public function getCiudadesPais($idPais)
    {
        try {
            if ($ciudad = Ciudades::where('idPais', '=', $idPais)->get()) {
                if (count($ciudad) == 0) {
                    $respuesta = "[" . json_encode(array('idCiudad' => 0, 'mensaje' => 'No hay datos para esta consulta')) . "]";
                } else {
                    $respuesta = $ciudad;
                }
            }
        } catch (Exception $e) {
            $respuesta = "[" . json_encode(array('idCiudad' => 0, 'mensaje' => 'Error')) . "]";
        }
        finally {
            return $respuesta;
        }
    }
    // Trae una ciudad segun su id
    public function getCiudadById($idCiudad){

        try{
            $ciudad = Ciudades::where('idCiudad','=',$idCiudad)->get();
            if($ciudad){
                if(count($ciudad) == 0){
                    $respuesta = "[".json_encode(array('idCiudad'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
                }else{
                   
                    $respuesta = $ciudad;
                }
            }
        }
        catch(Exception $e){
            $respuesta = "[".json_encode(array('idCiudad'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
            return $respuesta;
        }
    }
    //Trae los parallax por idioma y seccion
    public function getParallax($idIdioma, $seccion)
    {
        try {
            if ($parallax = SeccionesParallax::where('idIdioma', '=', $idIdioma)->where('seccion', '=', $seccion)->get()) {
                if (count($parallax) == 0) {
                    $respuesta = "[" . json_encode(array('idParallax' => 0, 'mensaje' => 'No hay datos para esta consulta')) . "]";
                } else {
                    $respuesta = $parallax;
                }
            }
        } catch (Exception $e) {
            $respuesta = "[" . json_encode(array('idParallax' => 0, 'mensaje' => 'Error')) . "]";
        }
        finally {
            return $respuesta;
        }
    }

    //Trae todas las ratus para el menu
    public function getRutas($idIdioma)
    {
        try {
            if ($ruta = Rutas::where('idIdioma', '=', $idIdioma)->get()) {
                if (count($ruta) == 0) {
                    $respuesta = "[" . json_encode(array('idRuta' => 0, 'mensaje' => 'No hay datos para esta consulta')) . "]";
                } else {
                    $respuesta = $ruta;
                }
            }
        } catch (Exception $e) {
            $respuesta = "[" . json_encode(array('idRuta' => 0, 'mensaje' => 'Error')) . "]";
        }
        finally {
            return $respuesta;
        }
    }
}
