<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Exception;
use App\SeccionesParallax;
use App\Paquetes;
use App\Idiomas;
use App\Paises;
use App\Ciudades;

class PaquetesController extends Controller{
	/*
	*	getPaquetes
    */
    public function getPaquetes($idIdioma){
        try{
            $paquetes = Paquetes::join('Paises','Paquetes.idPais','=','Paises.idPais')->join('Ciudades','Paquetes.idCiudad','=','Ciudades.idCiudad')->where('Paquetes.idIdioma','=',$idIdioma)->get(['Paquetes.*','Paises.nombre as PaisNombre','Ciudades.nombre as CiudadNombre']);
            if($paquetes){
            	if(count($paquetes) == 0){
            		$respuesta = "[".json_encode(array('idPaquete'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
            	}else{
                    foreach ($paquetes as $valor) {
                        $valor["precio"] = json_decode($valor["precio"]); 
                        $valor["preguntasFrecuentes"] = json_decode($valor["preguntasFrecuentes"]);
                        $valor["fecha"] = json_decode($valor["fecha"]);
                        $valor["seccionVIP"] = json_decode($valor["seccionVIP"]); 
                        $paquete[] = $valor;
                    }
            		$respuesta = $paquetes;
            	}
            }
        }
        catch(Exception $e){
        	$respuesta = "[".json_encode(array('idPaquete'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
        	return $respuesta;
        }
    }

    /*
	*	getPaquetesByPais
    */
    public function getPaquetesByPais($idIdioma,$idPais){
        try{
            $paquetes = Paquetes::join('Paises','Paquetes.idPais','=','Paises.idPais')->join('Ciudades','Paquetes.idCiudad','=','Ciudades.idCiudad')->where('Paquetes.idIdioma','=',$idIdioma)->where('Paquetes.idPais','=',$idPais)->get(['Paquetes.*','Paises.nombre as PaisNombre','Ciudades.nombre as CiudadNombre']);
            if($paquetes){
            	if(count($paquetes) == 0){
            		$respuesta = "[".json_encode(array('idPaquete'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
            	}else{
                    foreach ($paquetes as $valor) {
                        $valor["precio"] = json_decode($valor["precio"]); 
                        $valor["preguntasFrecuentes"] = json_decode($valor["preguntasFrecuentes"]);
                        $valor["fecha"] = json_decode($valor["fecha"]);
                        $valor["seccionVIP"] = json_decode($valor["seccionVIP"]); 
                        $paquete[] = $valor;
                    }
            		$respuesta = $paquetes;
            	}
            }
        }
        catch(Exception $e){
        	$respuesta = "[".json_encode(array('idPaquete'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
        	return $respuesta;
        }
    }

    /*
	*	getPaquetesByCiudad
    */
    public function getPaquetesByCiudad($idIdioma,$idCiudad){
        try{
            $paquetes = Paquetes::join('Paises','Paquetes.idPais','=','Paises.idPais')->join('Ciudades','Paquetes.idCiudad','=','Ciudades.idCiudad')->where('Paquetes.idIdioma','=',$idIdioma)->where('Paquetes.idCiudad','=',$idCiudad)->get(['Paquetes.*','Paises.nombre as PaisNombre','Ciudades.nombre as CiudadNombre']);
            if($paquetes){
            	if(count($paquetes) == 0){
            		$respuesta = "[".json_encode(array('idPaquete'=>0,'mensaje'=>'No hay datos para esta consulta'))."]";
            	}else{
                    foreach ($paquetes as $valor) {
                        $valor["precio"] = json_decode($valor["precio"]); 
                        $valor["preguntasFrecuentes"] = json_decode($valor["preguntasFrecuentes"]);
                        $valor["fecha"] = json_decode($valor["fecha"]);
                        $valor["seccionVIP"] = json_decode($valor["seccionVIP"]); 
                        $paquete[] = $valor;
                    }
            		$respuesta = $paquetes;
            	}
            }
        }
        catch(Exception $e){
        	$respuesta = "[".json_encode(array('idPaquete'=>0,'mensaje'=>'Error'))."]";
        }
        finally{
        	return $respuesta;
        }
    }
    

}	