<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquetes extends Model
{
    protected $table = 'Paquetes';
    protected $fillable = [];
    public $timestamps = false;
}
