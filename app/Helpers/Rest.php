<?php
//app/Helpers/Envato/User.php
namespace App\Helpers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Route;
  
class Rest {
    /**
     * @param string $url url '/API/consultarSlider/1'
     * @param string $metodo metodo GET
     * 
     * @return json
     */
    public static function consumirRest($url,$metodo) {
        
        $request = Request::create($url, $metodo); 
        
        $response = Route::dispatch($request); 
        //--Sobre carga para vaidar de acuerdo a mensaje de error....
        if(is_string($response->getOriginalContent())){
            $validador = (array) json_decode($response->getOriginalContent());
            if(isset($validador[0]->mensaje)){
                if(($validador[0]->mensaje=="No hay datos para esta consulta")||($validador[0]->mensaje=="Error"))
                    return $validador[0]->mensaje;                       
            }  
        }       

        //--
        $arr = $response->getOriginalContent();

        if(count($arr)>0){
            foreach ($arr as $valor) {
                $listado[] = $valor["attributes"];
            }
            return $listado;
        }else{
            return "No hay registros!";   
        }
    }
}