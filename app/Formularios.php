<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formularios extends Model
{
    protected $table = 'formularios';
    protected $fillable = [];
    public $timestamps = false;
}
