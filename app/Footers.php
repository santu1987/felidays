<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footers extends Model
{
    protected $table = 'footer';
    protected $fillable = [];
    public $timestamps = false;
}
