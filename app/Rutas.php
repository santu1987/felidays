<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rutas extends Model
{
    protected $table = 'rutas';
    protected $fillable = [];
    public $timestamps = false;
}
