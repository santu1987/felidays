<style>

    /*  bhoechie tab */
    div.bhoechie-tab-container {
        z-index: 10;
        background-color: #ffff;
        padding: 0 !important;
        border-radius: 4px;
        -moz-border-radius: 4px;
        border: 1px solid #ddd;
        margin-top: 20px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
        -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        background-clip: padding-box;
        opacity: 0.97;
        filter: alpha(opacity=97);
    }

    div.bhoechie-tab-menu {
        padding-right: 0;
        padding-left: 0;
        padding-bottom: 0;
    }

        div.bhoechie-tab-menu div.list-group {
            margin-bottom: 0;
        }

            div.bhoechie-tab-menu div.list-group > a {
                margin-bottom: 0;
                text-decoration: none;
            }

                div.bhoechie-tab-menu div.list-group > a .glyphicon,
                div.bhoechie-tab-menu div.list-group > a .fa {
                    color: #FF3300;
                }

                div.bhoechie-tab-menu div.list-group > a:first-child {
                    border-top-right-radius: 0;
                    -moz-border-top-right-radius: 0;
                }

                div.bhoechie-tab-menu div.list-group > a:last-child {
                    border-bottom-right-radius: 0;
                    -moz-border-bottom-right-radius: 0;
                }

                div.bhoechie-tab-menu div.list-group > a.active,
                div.bhoechie-tab-menu div.list-group > a.active .glyphicon,
                div.bhoechie-tab-menu div.list-group > a.active .fa {
                    background-color: #FF3300;
                    background-image: #FF3300;
                    color: #ffffff;
                }

                    div.bhoechie-tab-menu div.list-group > a.active:after {
                        content: '';
                        position: absolute;
                        left: 100%;
                        top: 50%;
                        margin-top: -13px;
                        border-left: 0;
                        border-bottom: 13px solid transparent;
                        border-top: 13px solid transparent;
                        border-left: 10px solid #FF3300 !important;
                    }

    .list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
        border-color: #FF3300 !important;
    }

    div.bhoechie-tab-content {
        background-color: #fff;
        /* border: 1px solid #eeeeee; */
        padding-left: 20px;
        padding-top: 10px;
    }

    div.bhoechie-tab div.bhoechie-tab-content:not(.active) {
        display: none;
    }

    .tituloTab {
        color: #FF3300 !important;
        font-weight: bold !important;
    }

    .active .tituloTab {
        color: #FFF !important;
        font-weight: bold !important;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                <div class="list-group">
                    <a href="#" class="list-group-item active text-center">
                        <span class="glyphicon glyphicon-info-sign" style="font-size:30px"></span>
                        <h4 class="tituloTab">Mas Informacion</h4>
                        <small>¿Quieres recibir nuestras ofertas?</small>
                        <small>¿Necesitas ayuda?</small>
                    </a>
                    <a href="#" class="list-group-item text-center">
                        <span class="glyphicon glyphicon-edit" style="font-size:30px"></span>
                        <h4 class="tituloTab">Reservas</h4>
                        <small>¿Deseas cancelar o cambiar?</small>
                        <small>¿Quieres hacer una reserva?</small>
                    </a>
                    <a href="#" class="list-group-item text-center">
                        <span class="glyphicon glyphicon glyphicon-globe" style="font-size:30px"></span>
                        <h4 class="tituloTab">Equipo</h4>
                        <small>¿Deseas pertenecer en nuestro equipo?</small>
                        <!-- <small>¿Quieres hacer una reserva?</small> -->
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    <div class="col-lg-12">
                        <div >
                            <form class="contact100-form validate-form" id="form1">
                                <!-- <span class="contact100-form-title">
                                    Send Us A Message
                                </span> -->
                                <label class="label-input100" for="asunto">Asunto</label>
                                <div class="wrap-input100 rSelect validate-input">
                                    <select class="select2 input100" style="width: 100%;" name="asunto" id="asunto">
                                        <option>Seleccione</option>
                                        <option>Problemas en la Web</option>
                                        <option>Recibir Ofertas</option>
                                        <option>Actividades</option>
                                        <option>Traslado</option>
                                        <option>Cotizaciones</option>
                                        <option>Reclamos</option>
                                    </select>
                                    
                                    <span class="focus-input100"></span>
                                </div>
                                <label class="label-input100" for="txtNombre">Ingrese su nombre *</label>
                                <div class="wrap-input100 rs1-wrap-input100 validate-input"
                                     data-validate="Ingrese su nombre">
                                    <input id="txtNombre" class="input100" type="text" name="txtNombre"
                                           placeholder="nombre">
                                    <span class="focus-input100"></span>
                                </div>
                                <div class="wrap-input100 rs2-wrap-input100 validate-input"
                                     data-validate="Ingrese su apellido">
                                    <input class="input100" type="text" name="txtApellido" placeholder="apellido">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="txtEmail">Ingrese su email *</label>
                                <div class="wrap-input100 validate-input" data-validate="Se requiere un Email valido: ex@abc.xyz">
                                    <input id="txtEmail" class="input100" type="text" name="txtEmail"
                                           placeholder="Eg. ejemplo@email.com">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="phone">Ingrese su Numero Telefonico</label>
                                <div class="wrap-input100">
                                    <input id="txtTelefono" class="input100" type="text" name="txtTelefono"
                                           placeholder="Ej. +1 800 000000">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="txtNombre">Ingrese su Pais</label>
                                <div class="wrap-input100 rSelect validate-input">
                                    <select class="select2 input100" style="width: 100%;" name="selectPais" id="selectPais">
                                        <option>Seleccione</option>
                                        @foreach($paises as $pais)
                                        <option value="{{$pais['idPais']}}">{{$pais['nombre']}}</option>
                                        @endforeach
                                    </select>
                                    
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="message">Mesaje *</label>
                                <div class="wrap-input100 validate-input" data-validate="El mensaje es requerido">
                                    <textarea id="txtMensaje" class="input100" name="txtMensaje" placeholder="Ingrese su mensaje"></textarea>
                                    <span class="focus-input100"></span>
                                </div>

                                <div class="container-contact100-form-btn col-lg-12">
                                    <div class="col-lg-6 text-left" style="padding:0">
                                        <div>
                                            <input type="checkbox" id="condiciones3" name="condiciones" data-parsley-mincheck="1" required="" data-parsley-multiple="condiciones">
                                            <label for="condiciones3" class="--inline"> Acepto
                                                <a class="js-lightbox" data-target-id="info" href="#" target="_blank" title="Política de privacidad">Política de privacidad</a>.
                                            </label>
                                        </div>
                                    </div>
                                    <div clas="col-lg-6">
                                        <button class="contact100-form-btn" id="btnForm1">
                                            Enviar
                                        </button>
                                    </div>
                                    
                                </div>
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                            </form>


                        </div>
                    </div>
                </div>
                <!-- train section -->
                <div class="bhoechie-tab-content">

                    <div class="bhoechie-tab-content active">
                    <div class="col-lg-12">
                        <div >
                            <form class="contact100-form validate-form">
                                <!-- <span class="contact100-form-title">
                                    Send Us A Message
                                </span> -->
                                <label class="label-input100" for="asunto">Asunto</label>
                                <div class="wrap-input100 rSelect validate-input">
                                    <select class="select2 input100" style="width: 100%;" name="asunto" id="asunto">
                                        <option>Seleccione</option>
                                        <option>Cancelar Reserva</option>
                                        <option>Modificar Reserva</option>
                                        <option>Consultar reserva</option>
                                        <option>Facturacion</option>
                                    </select>
                                    
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="txtCodigo">Ingrese los datos de su reserva *</label>
                                <div class="wrap-input100 validate-input rs1-wrap-input100" data-validate="Se requiere el codigo de la reserva">
                                    <input id="txtCodigo" class="input100" type="text" name="txtCodigo"
                                           placeholder="Codigo de reserva">
                                    <span class="focus-input100"></span>
                                </div>
                                <div class="wrap-input100 validate-input rs1-wrap-input100" data-validate="Se requiere el documento de identidad">
                                    <input id="txtDocumento" class="input100" type="text" name="txtDocumento"
                                           placeholder="documento de identidad">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="txtNombre">Ingrese su nombre *</label>
                                <div class="wrap-input100 rs1-wrap-input100 validate-input"
                                     data-validate="Ingrese su nombre">
                                    <input id="first-name" class="input100" type="text" name="nombre"
                                           placeholder="nombre">
                                    <span class="focus-input100"></span>
                                </div>
                                <div class="wrap-input100 rs2-wrap-input100 validate-input"
                                     data-validate="Ingrese su apellido">
                                    <input class="input100" type="text" name="txtApellido" placeholder="apellido">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="txtEmail">Ingrese su email *</label>
                                <div class="wrap-input100 validate-input" data-validate="Se requiere un Email valido: ex@abc.xyz">
                                    <input id="txtEmail" class="input100" type="text" name="txtEmail"
                                           placeholder="Eg. ejemplo@email.com">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="phone">Ingrese su Numero Telefonico</label>
                                <div class="wrap-input100">
                                    <input id="txtTelefono" class="input100" type="text" name="txtTelefono"
                                           placeholder="Ej. +1 800 000000">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="txtNombre">Ingrese su Pais</label>
                                <div class="wrap-input100 rSelect validate-input">
                                    <select class="select2 input100" style="width: 100%;" name="asunto" id="asunto">
                                        <option>Seleccione</option>
                                        @foreach($paises as $pais)
                                        <option value="{{$pais['idPais']}}">{{$pais['nombre']}}</option>
                                        @endforeach
                                    </select>
                                    
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="message">Mesaje *</label>
                                <div class="wrap-input100 validate-input" data-validate="El mensaje es requerido">
                                    <textarea id="txtMensaje" class="input100" name="txtMensaje" placeholder="Ingrese su mensaje"></textarea>
                                    <span class="focus-input100"></span>
                                </div>

                                <div class="container-contact100-form-btn col-lg-12">
                                    <div class="col-lg-6 text-left" style="padding:0">
                                        <div>
                                            <input type="checkbox" id="condiciones3" name="condiciones" data-parsley-mincheck="1" required="" data-parsley-multiple="condiciones">
                                            <label for="condiciones3" class="--inline"> Acepto
                                                <a class="js-lightbox" data-target-id="info" href="#" target="_blank" title="Política de privacidad">Política de privacidad</a>.
                                            </label>
                                        </div>
                                    </div>
                                    <div clas="col-lg-6">
                                        <button class="contact100-form-btn">
                                            Enviar
                                        </button>
                                    </div>
                                    
                                </div>
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                            </form>


                        </div>
                    </div>
                </div>

                </div>

                <!-- hotel search -->
                <div class="bhoechie-tab-content">
                    <div class="bhoechie-tab-content active">
                    <div class="col-lg-12">
                        <div >
                            <form class="contact100-form validate-form">
                                <!-- <span class="contact100-form-title">
                                    Send Us A Message
                                </span> -->
                                <label class="label-input100" for="asunto">Asunto</label>
                                <div class="wrap-input100 rSelect validate-input">
                                    <select class="select2 input100" style="width: 100%;" name="asunto" id="asunto">
                                        <option>Seleccione</option>
                                        <option>Quiero ser Guia</option>
                                        <option>Quiero ser Proveedor</option>
                                        <option>Oferta de trabajo</option>
                                    </select>
                                    
                                    <span class="focus-input100"></span>
                                </div>
                                
                                <label class="label-input100" for="txtNombre">Ingrese su nombre *</label>
                                <div class="wrap-input100 rs1-wrap-input100 validate-input"
                                     data-validate="Ingrese su nombre">
                                    <input id="first-name" class="input100" type="text" name="nombre"
                                           placeholder="nombre">
                                    <span class="focus-input100"></span>
                                </div>
                                <div class="wrap-input100 rs2-wrap-input100 validate-input"
                                     data-validate="Ingrese su apellido">
                                    <input class="input100" type="text" name="txtApellido" placeholder="apellido">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="txtEmail">Ingrese su email *</label>
                                <div class="wrap-input100 validate-input" data-validate="Se requiere un Email valido: ex@abc.xyz">
                                    <input id="txtEmail" class="input100" type="text" name="txtEmail"
                                           placeholder="Eg. ejemplo@email.com">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="phone">Ingrese su Numero Telefonico</label>
                                <div class="wrap-input100">
                                    <input id="txtTelefono" class="input100" type="text" name="txtTelefono"
                                           placeholder="Ej. +1 800 000000">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="txtNombre">Ingrese su Pais</label>
                                <div class="wrap-input100 rSelect validate-input">
                                    <select class="select2 input100" style="width: 100%;" name="asunto" id="asunto">
                                        <option>Seleccione</option>
                                        @foreach($paises as $pais)
                                        <option value="{{$pais['idPais']}}">{{$pais['nombre']}}</option>
                                        @endforeach
                                    </select>
                                    
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="txtDireccion">Ingrese su direccion</label>
                                <div class="wrap-input100 validate-input">
                                    <input id="txtDireccion" class="input100" type="text" name="txtDireccion"
                                           placeholder="direccion">
                                    <span class="focus-input100"></span>
                                </div>

                                <label class="label-input100" for="message">Mesaje *</label>
                                <div class="wrap-input100 validate-input" data-validate="El mensaje es requerido">
                                    <textarea id="txtMensaje" class="input100" name="txtMensaje" placeholder="Ingrese su mensaje"></textarea>
                                    <span class="focus-input100"></span>
                                </div>

                                <div class="container-contact100-form-btn col-lg-12">
                                    <div class="col-lg-6 text-left" style="padding:0">
                                        <div>
                                            <input type="checkbox" id="condiciones3" name="condiciones" data-parsley-mincheck="1" required="" data-parsley-multiple="condiciones">
                                            <label for="condiciones3" class="--inline"> Acepto
                                                <a class="js-lightbox" data-target-id="info" href="#" target="_blank" title="Política de privacidad">Política de privacidad</a>.
                                            </label>
                                        </div>
                                    </div>
                                    <div clas="col-lg-6">
                                        <button class="contact100-form-btn">
                                            Enviar
                                        </button>
                                    </div>
                                    
                                </div>
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                            </form>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@section('cuerpo_js')
<script>
$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });

    $("#btnForm1").on("click",function(){
        var form = $('#form1').serialize();
        var url='{{url('API/formularioRegistro')}}';
        alert(form)
        alert(url)
        $.ajax({
            url: url,
            type: 'POST',
            data: form,
            success: function (res) {
                alert(res)
            }
        })
    })
});
</script>
@endsection
