@extends('templates.main')

@section('title')
	Felidays - Guíamos tus emociones
@endsection

@section('cuerpo_css')
	<link rel="stylesheet" href="{{asset('stylesheets/felidays2.css')}}">
@endsection


@section('contenido')
	@include('contactos.parallaxTitulo',$parallax)
	@include('contactos.formularios',$paises)
@endsection

@section('cuerpo_js')

@endsection