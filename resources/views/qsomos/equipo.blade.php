<div class="pixfort_ebook_9" id="section_ebook_4">
		<div class="writers_section pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
                <p style="text-align: center;" class="moto-text_system_3 titulo-azul-bold">Conoce a nuestro equipo<br></p>
					<br>
					@php
						$contador=0;
					@endphp
					@foreach($equipo as $team)
						@php							
							$foto=$team["foto"];
						@endphp
						<div class="eight columns alpha">
							<div class="b_style">
								<img src="{{asset('images/equipo')}}/{{$foto}}" class="logo_style" alt="">
							</div>
							<div class="author_text">
								<h1 class="name_st editContent">{{$team["nombre"]}}</h1>
								<h6 class="job_st editContent">{{$team["funcion"]}}</h6>
								<p class="details_st editContent">{{$team["descripcion"]}}</p>
								
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>