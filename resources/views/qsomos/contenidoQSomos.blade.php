        <!--AQUI EMPIEZA QUIENES SOMOS-->
        @php
            $empresa=json_decode(json_encode($qsomos),true);
            $titulos= explode('*',$empresa[0]['titulo']);
        @endphp
        <div class="row-fixed f-bariol" data-widget="row">
            <div class="container-fluid">
                <div class="moto-cell col-sm-12" data-container="container">
                    <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                        <div class="moto-widget-spacer-block" style="height:30px"></div>
                    </div>
                    
                    <div class="moto-widget moto-widget-row" data-widget="row">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="moto-cell col-sm-1" data-container="container"></div>
                                <div class="moto-cell col-sm-10" data-container="container">
                                    <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                        <div class="moto-widget-spacer-block" style="height: 5px;"></div>
                                    </div>
                                    
                                    <div class="moto-widget moto-widget-spacer moto-preset-default                                                moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="masa">
                                        <div class="moto-widget-spacer-block" style="height: 0px;"></div>
                                    </div>
                                </div>
                                <div class="moto-cell col-sm-1" data-container="container"></div>
                            </div>
                        </div>
                    </div>
                    <!--MISION Y AWARDS-->
                    <div class="moto-widget moto-widget-row" data-widget="row">
                        <div class="container-fluid">
                            <div class="row">
                                <!--MISION-->
                                <div class="moto-cell col-sm-6" data-container="container">
                                    <div class="moto-widget moto-widget-row" data-widget="row">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="moto-cell col-sm-9" data-container="container">
                                                    <div data-animation="fadeInLeft" class="moto-widget moto-widget-text moto-preset-default                      wow fadeInLeft moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa">
                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                            <p style="text-align: right;" class="moto-text_system_8">
                                                                <a class="moto-link texto_naranja" data-action="url" target="_self" data-cke-saved-href="{{$empresa[0]['ruta']}}#sec1" href="{{$empresa[0]['ruta']}}#sec1">{{mb_strtoupper($titulos[1],'utf-8')}}</a><br>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div data-animation="fadeInLeft" class="moto-widget moto-widget-text moto-preset-default wow fadeInLeft moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                            <p style="text-align: right;" class="moto-text_normal">
                                                            {{$empresa[0]['mision']}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="moto-cell col-sm-3" data-container="container">
                                                    <div data-widget-id="wid__image__5b8743372b4e9" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto  wow fadeInLeft" data-widget="image">
                                                        <span class="moto-widget-image-link">
                                                            <img data-src="../images/mision.png" class="moto-widget-image-picture lazyload" data-id="150" title="" alt="">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                        <div class="moto-widget-spacer-block" style="height: 10px;"></div>
                                    </div>
                                </div>
                                <!--FIN MISION-->
                                <!--AWARDS-->
                                <div class="moto-cell col-sm-6" data-container="container">
                                    <div class="moto-widget moto-widget-row" data-widget="row">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="moto-cell col-sm-3" data-container="container">
                                                    <div data-widget-id="wid__image__5b8743372b77b" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto  wow fadeInRight" data-widget="image">
                                                        <span class="moto-widget-image-link">
                                                            <img data-src="../images/vision.png" class="moto-widget-image-picture lazyload" data-id="150" title="" alt="">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="moto-cell col-sm-9" data-container="container">
                                                    <div data-animation="fadeInRight" class="moto-widget moto-widget-text moto-preset-default                      wow fadeInRight moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa">
                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                            <p class="moto-text_system_8"><a class="moto-link texto_naranja" data-action="url" target="_self" data-cke-saved-href="{{$empresa[0]['ruta']}}#sec2" href="{{$empresa[0]['ruta']}}#sec2">{{mb_strtoupper($titulos[2],'utf-8')}}</a><br></p>
                                                        </div>
                                                    </div>
                                                    <div data-animation="fadeInRight" class="moto-widget moto-widget-text moto-preset-default                       wow fadeInRight moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                            <p class="moto-text_normal">{{$empresa[0]['vision']}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                        <div class="moto-widget-spacer-block" style="height: 10px;"></div>
                                    </div>
                                </div>
                                <!--FIN AWARDS-->
                            </div>
                        </div>
                    </div>
                    <!--FIN MISION Y AWARDS-->

                    <!--PRINCIPLES E HISTORIA-->
                    <div class="moto-widget moto-widget-row" data-widget="row">
                        <div class="container-fluid">
                            <div class="row">
                                <!--PRINCIPLES-->
                                <div class="moto-cell col-sm-6" data-container="container">
                                    <div class="moto-widget moto-widget-row" data-widget="row">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="moto-cell col-sm-9" data-container="container">
                                                    <div data-animation="fadeInLeft" class="moto-widget moto-widget-text moto-preset-default                      wow fadeInLeft moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa">
                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                            <p style="text-align: right;" class="moto-text_system_8"><a class="moto-link texto_naranja" data-action="url" target="_self" data-cke-saved-href="{{$empresa[0]['ruta']}}#sec3" href="{{$empresa[0]['ruta']}}#sec3">{{strtoupper($titulos[3])}}</a><br></p>
                                                        </div>
                                                    </div>
                                                    <div data-animation="fadeInLeft" class="moto-widget moto-widget-text moto-preset-default                       wow fadeInLeft moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                            <p style="text-align: right;" class="moto-text_normal">{{$empresa[0]['principio']}} </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="moto-cell col-sm-3" data-container="container">
                                                    <div data-widget-id="wid__image__5b8743372b9ee" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto  wow fadeInLeft" data-widget="image">
                                                        <span class="moto-widget-image-link">
                                                            <img data-src="../images/principios.png" class="moto-widget-image-picture lazyload" data-id="150" title="" alt="">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                        <div class="moto-widget-spacer-block" style="height: 10px;"></div>
                                    </div>
                                </div>
                                <!--FIN PRINCIPLES-->
                                <!--HISTORIA-->
                                <div class="moto-cell col-sm-6" data-container="container">
                                    <div class="moto-widget moto-widget-row" data-widget="row">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="moto-cell col-sm-3" data-container="container">
                                                    <div data-widget-id="wid__image__5b8743372bc50" class="moto-widget moto-widget-image moto-preset-default moto-align-center moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto  wow fadeInRight" data-widget="image">
                                                        <span class="moto-widget-image-link">
                                                            <img data-src="../images/historia.png" class="moto-widget-image-picture lazyload" data-id="150" title="" alt="">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="moto-cell col-sm-9" data-container="container">
                                                    <div data-animation="fadeInRight" class="moto-widget moto-widget-text moto-preset-default                      wow fadeInRight moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa">
                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                            <p class="moto-text_system_8"><a class="moto-link texto_naranja" data-action="url" target="_self" data-cke-saved-href="{{$empresa[0]['ruta']}}#sec4" href="{{$empresa[0]['ruta']}}#sec4">{{strtoupper($titulos[4])}}</a><br></p>
                                                        </div>
                                                    </div>
                                                    <div data-animation="fadeInRight" class="moto-widget moto-widget-text moto-preset-default                       wow fadeInRight moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                                        <div class="moto-widget-text-content moto-widget-text-editable">
                                                            <p class="moto-text_normal">{{$empresa[0]['historia']}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                        <div class="moto-widget-spacer-block" style="height: 10px;"></div>
                                    </div>
                                </div>
                                <!--FIN HISTORIA-->
                            </div>
                        </div>
                    </div>
                    <!--FIN PRINCIPLES E HISTORIA-->
                </div>
            </div>
        </div>
        <!--FIN QUIENES SOMOS-->