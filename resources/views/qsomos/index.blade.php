@extends('templates.main')

@section('title')
	Felidays - Guíamos tus emociones
@endsection

@section('cuerpo_css')
	<link rel="stylesheet" href="{{asset('stylesheets/felidays2.css')}}">
@endsection

@section('contenido')
	@include('qsomos.parallaxTitulo',$parallax)
	@include('qsomos.contenidoQSomos',$qsomos)
	@include('qsomos.equipo',$equipo)
@endsection

@section('cuerpo_js')

@endsection