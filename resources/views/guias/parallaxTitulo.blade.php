<div class="col-lg-12 padding0 m-top-85">
    <!--TITULO SEPARADOR -->
    <div class="big_padding pix_builder_bg dark event_bg" id="section_normal_4_1" style="background: linear-gradient(rgba(0,0,0,0.25),rgba(0,0,0,0.25)),url(/images/parallax/{{$parallax[0]['imagen']}}) #FF3300 fixed no-repeat">
        <div class="container">
            <div class="sixteen columns center_text">
                <p class="big_title editContent"><strong>{{$parallax[0]["titulo"]}}</strong></p>
                <div>
                <p class="normal_text max_600 editContent texto_blanco">
                    <strong>{{$parallax[0]["subtitulo"]}}</strong>
                </p>
                </div>
            </div>
       </div>
    </div>
    <!--FIN TITULO SEPARADOR-->
</div>