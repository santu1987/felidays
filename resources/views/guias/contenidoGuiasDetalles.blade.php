<!--SECCION DE GUIAS-->
<form class="form-guias">
<div class="center_text big_padding pix_builder_bg cuerpo_guias fondo_gris" id="section_intro_title" >
    <div id="contenedor-guias" class="container ">
        <div class="col-12">
	        <!-- Widget: user widget style 1 -->
	        <div class="box box-widget widget-user">
		        <!-- Add the bg color to the header using any of the bg-* classes -->
		        <div class="widget-user-header bg-black" style="background: url('/images/fondos_guias/{{$guias[0]['imagenFondo']}}') center center;">
		          <h5 class="widget-user-desc"></h5>
		        </div>
		        <div class="widget-user-image">
		          <img class="img-circle" src="/images/guias/{{$guias[0]['imagen']}}" alt="User Avatar">
		        </div>
		        <div class="box-footer">
		          	<div class="row">
						<div class="col-sm-4 border-right">
							<div class="description-block">
							    <h5 class="description-header">{{$guias[0]['nombreCiudad']}}</h5>
							    <span class="description-text">
							    	<img src="/images/banderas/{{$guias[0]['bandera']}}" alt="spain" height="18" width="18" />
							    </span>
							</div>
						  	<!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 border-right">
							<h3 class="widget-user-username">{{$guias[0]["nombre"]}}</h3>
						</div>
						<!-- /.col -->
						<div class="col-sm-4 border-right">
							<div class="description-block">
								<h5 class="description-header">Valoración</h5>
								<span class="description-text">
									<div class="col-lg-12 padding0">
				                        <span class="text-left c-naranja-oscuro">
				                            @for ($i=1;$i<=5;$i++)
				                                @if($i<=$guias[0]['evaluacion'])
				                                    <span class="fa fa-star"></span>
				                                @elseif($i>$guias[0]['evaluacion'])
				                                    <span class="fa fa-star-o"></span>
				                                @endif
				                            @endfor
				                            <!--<span class="fa fa-star-o"></span>
				                            <span class="fa fa-star-o"></span>-->
				                        </span>
				                    </div>
								</span>
							</div>
							<!-- /.description-block -->
						</div>
						<!-- /.col -->
		          	</div>
		          <!-- /.row -->
		        </div>
	        </div>
	        <!-- /.widget-user -->
        </div>
		<div class="card card-guias">
			<div class="card-body">
			    <p class="card-text">{{$guias[0]["descripcion"]}}</p>
			</div>
		</div>
    </div>
</div>
</form>
<!-- -->