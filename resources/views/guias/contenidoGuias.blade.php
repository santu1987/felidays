<!--SECCION DE GUIAS-->
<form>
<div class="center_text big_padding pix_builder_bg " id="section_intro_title">
    <div id="contenedor-guias" class="container " data="{{$filtros['offset']}}|{{$filtros['limit']}}">
        <!--GUIA 1-->
        @foreach ($guias as $guia)
        <div>
            <div class="one-third column">
                <div class="simple_team">
                    <div class="simple_team_img ">
                        <img alt="" src="/images/guias/{{$guia['imagen']}}">
                    </div>
                    <div>
                        <h4 class="editContent gray">
                            <strong>{{$guia['nombre']}}</strong>
                        </h4>
                        <p class="editContent small_text light_gray">
                            {{substr($guia['descripcion'],0,100)}}
                        </p>
                        <div class="col-md-6 text-right texto-opiniones">
                            <div class="col-lg-12 padding0">
                                <span class="text-left c-naranja-oscuro">
                                    @for ($i=1;$i<=5;$i++)
                                        @if($i<=$guia['evaluacion'])
                                            <span class="fa fa-star"></span>
                                        @elseif($i>$guia['evaluacion'])
                                            <span class="fa fa-star-o"></span>
                                        @endif
                                    @endfor
                                    <!--<span class="fa fa-star-o"></span>
                                    <span class="fa fa-star-o"></span>-->
                                </span>
                            </div>
                            
                        </div>
                        <div class="col-md-6 text-left padding0">
                            <img src="/images/banderas/{{$guia['bandera']}}" alt="spain" height="18" width="18" />
                            <span class="texto-opiniones">
                                {{$guia["nombreCiudad"]}}
                            </span>
                        </div>
                        <div class="col-12 btn-guias">
                            <a href="/guias/{{$guia['idioma']}}/{{$guia['idGuia']}}" class="pix_button btn_normal small_wide_button dark slow_fade light_blue_bg editContent small_bold">
                                Leer mas
                            </a>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <!--FIN GUIA 1-->
    </div>
</div>
<div class="text-center big_padding2" style="margin-bottom: 50px;">
    <div id="btn_ver_mas" class="pix_button btn_normal small_wide_button dark slow_fade light_blue_bg editContent small_bold" style="cursor:pointer" ><strong>Ver mas guias</strong></div>
</div>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<!--FIN SECCION DE GUIAS-->
<div class="container">
    <div class="col-12">
        <div id="cuerpoMensaje" name=cuerpoMensaje>
        </div>
    </div>
</div>
</form>