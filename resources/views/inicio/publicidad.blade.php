<div id="themeSlider2" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#themeSlider2" data-slide-to="0" class="active"></li>
        <li data-target="#themeSlider2" data-slide-to="1"></li>
        <li data-target="#themeSlider2" data-slide-to="2"></li>
    </ol>

    <div class="carousel-inner">
        <!--SLIDER 1-->
        <div class="item active">
            <div class="big_padding pix_builder_bg fondo_gris " id="section_normal_4_1">
                <div class="container">
                    <div class="sixteen columns">
                        <div class="eight columns alpha margin_bottom_30">
                            <span class="bold_text big_text blue_text editContent">BUSCADOR DE DESTINO</span>
                            <p class="big_title texto_negro bold_text editContent">Dinos hacia dónde quieres ir!</p>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <a class="pix_button btn_normal pix_button_flat blue_bg bold_text" href="#">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span>Llévame</span>
                            </a>

                        </div>
                        <div class="eight columns omega pix_container">
                            <img src="{{asset('images/cards/roma1.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--FIN SLIDER 1-->
        <!-- SLIDER 2-->
        <div class="item">
            <div class="big_padding pix_builder_bg fondo_gris " id="section_normal_4_2">
                <div class="container ">
                    <div class="sixteen columns">
                        <div class="eight columns alpha margin_bottom_30">
                            <span class="bold_text big_text blue_text editContent">GET FREE SUPPORT</span>
                            <p class="big_title texto_negro bold_text editContent">Get The Most Amazing Builder!</p>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <a class="pix_button btn_normal pix_button_flat blue_bg bold_text" href="#">
                                <i class="pi pixicon-download"></i>
                                <span>GET SOFTWARE</span>
                            </a>

                        </div>
                        <div class="eight columns omega pix_container">
                            <img src="{{asset('images/cards/roma1.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--FIN SLIDER 2-->
        <!-- SLIDER 3-->
        <div class="item">
            <div class="big_padding pix_builder_bg fondo_gris " id="section_normal_4_3">
                <div class="container ">
                    <div class="sixteen columns">
                        <div class="eight columns alpha margin_bottom_30">
                            <span class="bold_text big_text blue_text editContent">GET FREE SUPPORT</span>
                            <p class="big_title texto_negro bold_text editContent">Get The Most Amazing Builder!</p>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <a class="pix_button btn_normal pix_button_flat blue_bg bold_text" href="#">
                                <i class="pi pixicon-download"></i>
                                <span>GET SOFTWARE</span>
                            </a>

                        </div>
                        <div class="eight columns omega pix_container">
                            <img src="{{asset('images/cards/roma1.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--FIN SLIDER 3-->
    </div>
</div>