@extends('templates.main')

@section('title')
	Felidays - Guíamos tus emociones
@endsection

@section('cuerpo_css')
	<link rel="stylesheet" href="{{asset('stylesheets/felidays2.css')}}">
@endsection

@section('contenido')
	@include('inicio.slider',$sliders)
	@include('inicio.quienesSomos',$qsomos)
	@include('inicio.destinos')
	@include('inicio.guias',$guias)
	@include('inicio.publicidad')
@endsection

@section('cuerpo_js')

@endsection