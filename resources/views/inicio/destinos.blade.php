<div class="pixfort_hotel_5 " id="section_hotel_4_1">
            <div class="Homes pix_builder_bg fondo_gris">
                <div class="container">
                    <div class="sixteen columns">
                        <!--<h1 class="title_homes editContent titulo-azul-bold">Destinos</h1>
                        <p class="subtitle_homes editContent c-gris1">
                            Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore.
                        </p>-->
                        <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default wow fadeIn moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="maaa">
                            <div class="moto-widget-text-content moto-widget-text-editable">
                                <p style="text-align: center;" class="moto-text_system_3 titulo-azul-bold">Destinos<br></p>
                            </div>
                        </div>
                        <div class="moto-widget moto-widget-row" data-widget="row">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="moto-cell col-sm-1" data-container="container"></div>
                                    <div class="moto-cell col-sm-10" data-container="container">
                                        <div class="moto-widget moto-widget-spacer moto-preset-default" data-widget="spacer" data-preset="default" data-spacing="">
                                            <div class="moto-widget-spacer-block" style="height: 5px;"></div>
                                        </div>
                                        <div data-animation="fadeIn" class="moto-widget moto-widget-text moto-preset-default                         wow fadeIn moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sama">
                                            <div class="moto-widget-text-content moto-widget-text-editable">
                                                <p style="text-align: center;" class="moto-text_system_9">
                                                    <span class="moto-color5_4">
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                                                    </span><br>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="moto-widget moto-widget-spacer moto-preset-default                                                moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="masa">
                                            <div class="moto-widget-spacer-block" style="height: 0px;"></div>
                                        </div>
                                    </div>
                                    <div class="moto-cell col-sm-1" data-container="container"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- DESTINO 1 -->
                            <div class="col-sm-6 col-md-4 col-lg-4  wow fadeIn " data-animation="fadeIn">
                                <div class="project">
                                    <span  class="mask">
                                        <span class="info">
                                            <h3>Roma</h3>
                                        </span>  

                                        <span class="texto-opiniones2">
                                            <h4>430 opiniones</h4>
                                        </span>
                                    
                                        
                                        <span class="btn-destino btn-see-project">
                                            Ver destino
                                        </span>
                                    </span>
                                    <span> 
                                        <div class="thumbnail2">
                                            <img src="images/cards/1.jpg" alt="prueba" class="img-card">
                                            <div class="caption">
                                                <h3 class="text-left c-naranja-oscuro">Roma</h3>
                                                <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                                    <div class="col-lg-12 padding0">
                                                        <span class="text-left c-naranja-oscuro">
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        </span>
                                                    </div>
                                                    <div class="col-lg-12 padding0">
                                                        <span class="texto-opiniones2">
                                                            430 opiniones
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-right padding0">
                                                    <span class="text-right texto-costo ">
                                                        <span class="fa fa-eur" aria-hidden="true"></span>
                                                        120,80
                                                    </span>
                                                </div>
                                                <div class="col-lg-12 padding0 texto-cards">
                                                    <span>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                                    </span>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    </span>
                                </div>    
                            </div>
                            <!-- FIN DESTINO 1 -->
                            <!-- DESTINO 2 -->
                            <div class="col-sm-6 col-md-4 col-lg-4  wow fadeInUp " data-animation="fadeInUp">
                                <div class="project">
                                    <span  class="mask">
                                        <span class="info">
                                            <h3>París</h3>
                                        </span>  

                                        <span class="texto-opiniones2">
                                            <h4>210 opiniones</h4>
                                        </span>
                                    
                                        
                                        <span class="btn-destino btn-see-project">
                                            Ver destino
                                        </span>
                                    </span>
                                    <span> 
                                        <div class="thumbnail2">
                                            <img src="images/cards/2.jpg" alt="prueba" class="img-card">
                                            <div class="caption">
                                                <h3 class="text-left c-naranja-oscuro">París</h3>
                                                <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                                    <div class="col-lg-12 padding0">
                                                        <span class="text-left c-naranja-oscuro">
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                        </span>
                                                    </div>
                                                    <div class="col-lg-12 padding0">
                                                        <span class="texto-opiniones">
                                                            210 opiniones
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-right padding0">
                                                    <span class="text-right texto-costo ">
                                                        <span class="fa fa-eur" aria-hidden="true"></span>
                                                        220,89
                                                    </span>
                                                </div>
                                                <div class="col-lg-12 padding0 texto-cards">
                                                    <span>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                                    </span>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    </span>        
                                </div>
                            </div>
                            <!-- FIN DESTINO 2 -->
                            <!-- DESTINO 3 -->
                            <div class="col-sm-6 col-md-4 col-lg-4  wow fadeInUp " data-animation="fadeInUp">
                                <div class="project">
                                    <span  class="mask">
                                        <span class="info">
                                            <h3>Manchester</h3>
                                        </span>  

                                        <span class="texto-opiniones2">
                                            <h4>40 opiniones</h4>
                                        </span>
                                    
                                        
                                        <span class="btn-destino btn-see-project">
                                            Ver destino
                                        </span>
                                    </span>
                                    <span> 
                                        <div class="thumbnail2">
                                            <img src="images/cards/3.jpg" alt="prueba" class="img-card">
                                            <div class="caption">
                                                <h3 class="text-left c-naranja-oscuro">Manchester</h3>
                                                <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                                    <div class="col-lg-12 padding0">
                                                        <span class="text-left c-naranja-oscuro">
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        </span>
                                                    </div>
                                                    <div class="col-lg-12 padding0">
                                                        <span class="texto-opiniones">
                                                            40 opiniones
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-right padding0">
                                                    <span class="text-right texto-costo ">
                                                        <span class="fa fa-eur" aria-hidden="true"></span>
                                                        420,00
                                                    </span>
                                                </div>
                                                <div class="col-lg-12 padding0 texto-cards">
                                                    <span>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                                    </span>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    </span>
                                </div>        
                            </div>
                            <!-- FIN DESTINO 3 -->
                            <!-- DESTINO 4 -->
                            <div class="col-sm-6 col-md-4 col-lg-4  wow fadeInUp " data-animation="fadeInUp">
                                <div class="project">
                                    <span  class="mask">
                                        <span class="info">
                                            <h3>Londres</h3>
                                        </span>  

                                        <span class="texto-opiniones2">
                                            <h4>130 opiniones</h4>
                                        </span>
                                    
                                        
                                        <span class="btn-destino btn-see-project">
                                            Ver destino
                                        </span>
                                    </span>
                                    <span> 
                                        <div class="thumbnail2">
                                            <img src="images/cards/4.jpg" alt="prueba" class="img-card">
                                            <div class="caption">
                                                <h3 class="text-left c-naranja-oscuro">Londres</h3>
                                                <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                                    <div class="col-lg-12 padding0">
                                                        <span class="text-left c-naranja-oscuro">
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        </span>
                                                    </div>
                                                    <div class="col-lg-12 padding0">
                                                        <span class="texto-opiniones">
                                                            130 opiniones
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-right padding0">
                                                    <span class="text-right texto-costo ">
                                                        <span class="fa fa-eur" aria-hidden="true"></span>
                                                        190,90
                                                    </span>
                                                </div>
                                                <div class="col-lg-12 padding0 texto-cards">
                                                    <span>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                                    </span>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    </span>
                                </div>        
                            </div>
                            <!-- FIN DESTINO 4 -->
                            <!-- DESTINO 5 -->
                            <div class="col-sm-6 col-md-4 col-lg-4  wow fadeInUp " data-animation="fadeInUp">
                                <div class="project">
                                    <span  class="mask">
                                        <span class="info">
                                            <h3>Madrid</h3>
                                        </span>  

                                        <span class="texto-opiniones2">
                                            <h4>89 opiniones</h4>
                                        </span>
                                    
                                        
                                        <span class="btn-destino btn-see-project">
                                            Ver destino
                                        </span>
                                    </span>
                                    <span> 
                                        <div class="thumbnail2">
                                            <img src="images/cards/5.jpg" alt="prueba" class="img-card">
                                            <div class="caption">
                                                <h3 class="text-left c-naranja-oscuro">Madrid</h3>
                                                <div class="col-md-6 text-left p-left-0 ">
                                                    <div class="col-lg-12 padding0">
                                                        <span class="text-left c-naranja-oscuro">
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                        </span>
                                                    </div>
                                                    <div class="col-lg-12 padding0">
                                                        <span class="texto-opiniones">
                                                            89 opiniones
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-right padding0">
                                                    <span class="text-right texto-costo ">
                                                        <span class="fa fa-eur" aria-hidden="true"></span>
                                                        105,90
                                                    </span>
                                                </div>
                                                <div class="col-lg-12 padding0 texto-cards">
                                                    <span>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                                    </span>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    </span>
                                </div>        
                            </div>
                            <!-- FIN DESTINO 5 -->
                            <!-- DESTINO 6 -->
                            <div class="col-sm-6 col-md-4 col-lg-4  wow fadeInUp " data-animation="fadeInUp">                               
                                <div class="project">
                                    <span  class="mask">
                                        <span class="info">
                                            <h3>New York</h3>
                                        </span>  

                                        <span class="texto-opiniones2">
                                            <h4>50 opiniones</h4>
                                        </span>
                                    
                                        
                                        <span class="btn-destino btn-see-project">
                                            Ver destino
                                        </span>
                                    </span>
                                    <span> 
                                        <div class="thumbnail2">
                                            <img src="images/cards/6.jpg" alt="prueba" class="img-card">
                                            <div class="caption">
                                                <h3 class="text-left c-naranja-oscuro">New York</h3>
                                                <div class="col-md-6 text-left p-left-0 texto-opiniones">
                                                    <div class="col-lg-12 padding0">
                                                        <span class="text-left c-naranja-oscuro">
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                            <span class="fa fa-star-o"></span>
                                                        </span>
                                                    </div>
                                                    <div class="col-lg-12 padding0">
                                                        <span class="texto-opiniones">
                                                            50 opiniones
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-right padding0">
                                                    <span class="text-right texto-costo ">
                                                        <span class="fa fa-eur" aria-hidden="true"></span>
                                                        14,40
                                                    </span>
                                                </div>
                                                <div class="col-lg-12 padding0 texto-cards">
                                                    <span>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer interdum accumsan rutrum.
                                                    </span>
                                                </div>
                                                <div style="clear:both"></div>
                                            </div>
                                        </div>
                                    </span>        
                                </div>
                            </div>
                            <!-- FIN DESTINO 6 -->
                        </div>
                    </div>
                    <div class="text-center big_padding2" style="margin-bottom: 50px;">
                <a class="pix_button btn_normal small_wide_button dark slow_fade light_blue_bg editContent small_bold" style="cursor:pointer" href="#"><strong>Ver mas destinos</strong></a>
            </div>
                
                </div>
            </div>
            
        </div>
        