        <!--TITULO SEPARADOR -->
        <div class="big_padding pix_builder_bg dark event_bg" id="section_normal_4_1" style="background: linear-gradient(rgba(0,0,0,0.25),rgba(0,0,0,0.25)),url(images/parallax/{{$parallaxGuias[0]['imagen']}}) #FF3300 fixed no-repeat">
            <div class="container">
                <div class="sixteen columns center_text">
                    <p class="big_title editContent"><strong>{{$parallaxGuias[0]["titulo"]}}</strong></p>
                    <div>
                    <p class="normal_text max_600 editContent texto_blanco">
                        <strong>{{$parallaxGuias[0]["subtitulo"]}}</strong>
                    </p>
                    </div>
                </div>
           </div>
        </div>
        <!--FIN TITULO SEPARADOR-->

        <!--SECCION DE GUIAS-->
        <div class="center_text big_padding pix_builder_bg" id="section_intro_title">
            <div class="container ">
            <!--GUIA-->
            @foreach ($guias as $guia)
            <div>
                <div class="one-third column">
                    <div class="simple_team">
                        <div class="simple_team_img ">
                            <img alt="" src="/images/guias/{{$guia['imagen']}}">
                        </div>
                        <div>
                            <h4 class="editContent gray">
                                <strong>{{$guia['nombre']}}</strong>
                            </h4>
                            <p class="editContent small_text light_gray">
                               {{substr($guia['descripcion'],0,100)}}
                            </p>
                            <div class="col-md-6 text-right texto-opiniones">
                                <div class="col-lg-12 padding0">
                                    <span class="text-left c-naranja-oscuro">
                                        @for ($i=1;$i<=5;$i++)
                                            @if($i<=$guia['evaluacion'])
                                                <span class="fa fa-star"></span>
                                            @elseif($i>$guia['evaluacion'])
                                                <span class="fa fa-star-o"></span>
                                            @endif
                                        @endfor
                                        <!--<span class="fa fa-star-o"></span>
                                        <span class="fa fa-star-o"></span>-->
                                    </span>
                                </div>
                                
                            </div>
                            <div class="col-md-6 text-left padding0">
                                <img src="/images/banderas/{{$guia['bandera']}}" alt="spain" height="18" width="18" />
                                <span class="texto-opiniones">
                                    {{$guia["nombreCiudad"]}}
                                </span>
                            </div>
                            <div class="col-12 btn-guias">
                                <a href="/guias/{{$guia['idioma']}}/{{$guia['idGuia']}}" class="pix_button btn_normal small_wide_button dark slow_fade light_blue_bg editContent small_bold">
                                    Leer mas
                                </a>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!--FIN GUIA -->
            </div>
        </div>
        <div class="text-center big_padding2" style="margin-bottom: 50px;">
            <a class="pix_button btn_normal small_wide_button dark slow_fade light_blue_bg editContent small_bold" style="cursor:pointer" href="/guias/es"><strong>Ver mas guias</strong></a>
        </div>

        <!--FIN SECCION DE GUIAS-->