        <!-- Slider... -->
        <div class="col-lg-12 padding0 m-top-85">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <!--row_btn_slider-->
                    @php
                        $count1 = 0;
                        $count2 = 0;
                        $classActive="";
                    @endphp
                    @foreach ($sliders as $slider)
                        @if($count1==0)
                            @php
                                $classActive="active";
                            @endphp
                        @else   
                            @php  
                                $classActive="";
                            @endphp
                        @endif
                        <li id="slide_{{$count1}}" data-target="#carousel-example-generic" data-slide-to="{{$count1}}" class="{{$classActive}}"></li>
                        @php
                            $count1++;
                        @endphp
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    <!--row_slider-->
                    @foreach ($sliders as $slider)
                        @if($count2==0)
                            @php
                                $classActive="active";
                            @endphp
                        @else 
                            @php  
                                $classActive="";
                            @endphp
                        @endif
                        <div class="item {{$classActive}}" id="div_carousel_{{$count2}}" role="listbox">
                            <!--Para grandes pantallas -->
                            <div class="g-bg-cover   g-bg-black-opacity-0_3--after tamano_slide">
                                <img src="images/{{$slider['imagen']}}" alt="" style="height:100%;" class=" img-fluid d-block g-bg-pos-top-center g-bg-img-hero">
                            </div>
                            <!-- -->
                            <div class="header-text ">
                                <div id="" class="texto_slide container g-z-index-1">
                                    <div class="g-max-width-600">
                                        <h2>
                                            <span>{{$slider['titulo']}}</span>
                                        </h2>
                                        <br>
                                        @php 
                                            $boton=json_decode(json_encode($slider['boton']),true);                                            
                                        @endphp
                                        @if($boton["nombre"]!="")
                                            <div class="carousel-btns">
                                                <a class="btn btn-md btn-default boton1" href="{{$boton['url']}}">{{$boton['nombre']}}</a>
                                            </div>                                                                                      
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @php
                            $count2++;
                        @endphp
                    @endforeach
                                        
                    <!--row_slider-->
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- -->

        