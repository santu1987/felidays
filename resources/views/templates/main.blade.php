<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title','Default')</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
	<meta http-equiv="x-ua-compatible" content="IE=9">
	<link rel="shortcut icon" href="{{{ asset('images/main/favicon.ico') }}}">
    <!-- Cuerpo de css -->
    <link rel="stylesheet" href="{{asset('stylesheets/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/assets.min9e70.css?_build=1534856391')}}" />
    <link rel="stylesheet" href="{{asset('stylesheets/stylesa084.css?_build=1535590879')}}" />
    <link rel="stylesheet" href="{{asset('stylesheets/styles2faa.css?_build=1534859320')}}" id="moto-website-style" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('stylesheets/menu.css')}}">
   <!-- <link rel="stylesheet" href="stylesheets/flat-ui-slider.css"> -->
    <link rel="stylesheet" href="{{asset('stylesheets/base.css')}}"> 
    <link rel="stylesheet" href="{{asset('stylesheets/skeleton.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/landings.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/main.css')}}">
	<link rel="stylesheet" href="{{asset('stylesheets/mainform.css')}}">
	<link rel="stylesheet" href="{{asset('stylesheets/util.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/landings_layouts.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/box.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/pixicon.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/framework.css')}}">
    <link rel="stylesheet" href="{{asset('stylesheets/carrousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animations.min.css')}}" type="text/css" media="all" /> 
    <link rel="stylesheet" href="{{asset('assets/css/checkbox.css')}}" type="text/css" media="all" /> 
    <link rel="stylesheet" href="{{asset('assets/css/range.css')}}" type="text/css" media="all" /> 

    <link rel="stylesheet" href="{{asset('stylesheets/felidays1.css')}}">
    <!-- <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome/css/font-awesome.min.css')}}" type="text/css" media="all" />
    @yield('cuerpo_css')
    <!-- Fin cuerpo css -->
    <!--JS que no se porque van en el inicio.... -->
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<!-- -->
	<!--FONTS FELIDAYS-->
	<style type="text/css">
	@font-face {
		font-family: Bariol Light;
		src: url('{{ public_path('fonts/Bariol_Light.otf') }}');
	}
</style>
</head>
<body>

	<div class="page" id="page">
		<div class="header_nav_1 dark" id="section_intro_3">
			@include('templates.partials.header')
		</div>
		<!--Contenido de la web -->
		@yield('contenido')
		<!-- Fin de contenido de la web-->
		
		<!--Footer -->
		@include('templates.partials.footer')
		<!-- -->
	</div>	
	<!-- Cuerpo js -->
	
	
	<script src="{{asset('assets/js/website.assets.min7c7f.js?_build=1534856457')}}" type="text/javascript" data-cfasync="false"></script>
	
	<script src="{{asset('assets/js/website.min32ab.js?_build=1534856445')}}" type="text/javascript" data-cfasync="false"></script>
	<script src="{{asset('js-files/jquery.easing.1.3.js')}}" type="text/javascript"></script>
	<script src="{{asset('js-files/jquery.common.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('js-files/ticker.js')}}" type="text/javascript"></script>
	<script src="{{asset('js-files/custom1.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/smoothscroll.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/appear.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('js-files/jquery.ui.touch-punch.min.js')}}"></script>
	<!-- <script src="js-files/bootstrap.min.js"></script> -->
	<script src="{{asset('js-files/bootstrap-switch.js')}}"></script>
	<script src="{{asset('js-files/custom3.js')}}" type="text/javascript"></script>

	<script src="{{asset('assets/js/appear.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/animations.js')}}" type="text/javascript"></script>
	<!-- <script src="{{asset('assets/js/fbasic.js')}}" type="text/javascript"></script> --> 
	<script src="{{asset('assets/js/felidays.js')}}" type="text/javascript"></script> 
	<script src="{{asset('assets/js/range.js')}}" type="text/javascript"></script>  
	<!-- -->
	@yield('cuerpo_js')
</body>