 <style>
.selectIdioma{
    width: 50px;
    margin-top: 25px;
    height: 25px;
    margin-left: 10px;
}
 </style>
 <div class="">
    <div class="container2">
        <div class="sixteen columns firas2">
            <nav class="navbar navbar-white f-bariol f-bariol-20 navbar-embossed navbar-lg pix_nav_1" role="navigation">
                <div class="containerss">
                    <div class="navbar-header col-md-4 text-center col-xs-12">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse-02">
                            <span class="sr-only">Menu</span>
                        </button>
                        
                        <a href="{{asset('/')}}{{$Idioma}}" class="text-left">
                            <img class="pix_nav_logo" alt="" src="{{asset('images/main/logofelidays3.png')}}"> 
                        </a>  
                    </div>
                    <div class="collapse navbar-collapse col-md-8 text-center" id="navbar-collapse-02">
                        <ul class="nav navbar-nav navbar-rigth">
                            @php $count=0; @endphp
                            @foreach($menu as $menue)
                                @if($count <=4 )
                                    <li class="propClone"><a href="{{asset('')}}{{$menue['ruta']}}">{{$menue['nombre']}}</a></li>
                                @endif
                                @php $count++; @endphp
                            @endforeach                           
                            
                            <!--INICIE SESION--> 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-user"></span> 
                                    <strong>{{$menu[5]['nombre']}}</strong>
                                    <span class="glyphicon glyphicon-chevron-down"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="navbar-login col-md-12">
                                            <div class="row">                                                
                                                <div class="col-md-12">
                                                    <h4 class="text-center">Mi Perfil</h4>
                                                    <form>
                                                        <div class="col-md-12">
                                                            <p>¿Ya tienes cuenta?</p>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Email</label>
                                                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="examplePass1">Password</label>
                                                                <input type="password" class="form-control" id="examplePass1" placeholder="Password">
                                                            </div>
                                                            <a href="#" class="col-md-12 text-rigth" style="color:#000">He olvidado mi clave</a>
                                                            <br>
                                                        </div>
                                                    </form>
                                                    
                                                    <div class="col-md-12 text-center" style="margin-top:30px">

                                                        <a class="btn btn-lg btn-primary"  href="http://dotstrap.com/">
                                                        <i class="glyphicon glyphicon-user pull-left"></i><span>Iniciar Sesion con Facebook</span></a>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    
                                </ul>
                            </li>  
                            <!--INICIE SESION FIN--> 
                            <li class="propClone">
                                <select id="selectIdiomas" onchange="cambiarIdioma(this);" class="selectIdioma">
                                    <option value="es">ES</option>
                                    <option value="en">EN</option>
                                </select>
                            </li>
                            <input type="hidden" value="{{$Idioma}}" id="idiomaSeleccionado" >
                        </ul>
                        
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </nav>
        </div>
    </div><!-- container -->
</div>

