        <div class="pixfort_text_4 darking pix_builder_bg" id="section_text_4">
            <div class="footer3">
                <div class="container ">
                    <div class="five columns alpha">
                        <div class="content_div area_1">
                            <img class="pix_footer_logo" alt="" src="{{asset('images/main/footer-logo-3.png')}}">
                            <p class="small_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt.</p>
                            <ul class="bottom-icons">
                                <li><a class="pi pixicon-facebook2" href="#fakelink"></a></li>
                                <li><a class="pi pixicon-twitter2" href="#fakelink"></a></li>
                                <li><a class="pi pixicon-instagram" href="#fakelink"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="three columns">
                        <div class="content_div area_2">
                            <span class="pix_text"><span class="editContent footer3_title">Navigation</span></span>
                            <ul class="footer3_menu">
                                <li><a class="pix_text" href="#"><span class="editContent">Home</span></a></li>
                                <li><a class="pix_text" href="#"><span class="editContent">Overview</span></a></li>
                                <li><a class="pix_text" href="#"><span class="editContent">About</span></a></li>
                                <li><a class="pix_text" href="#"><span class="editContent">Buy now</span></a></li>
                                <li><a class="pix_text" href="#"><span class="editContent">support</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="four columns">
                        <div class="content_div area_3">
                            <span class="pix_text"><span class="editContent big_number">347 567 78 90</span></span>
                            <span class="pix_text"><span class="editContent small_bold light_color">AVAILABLE FROM 12PM - 18PM</span></span>
                            <h4 class="editContent med_title">New York, NY</h4>
                            <p class="editContent small_bold">560 Judah St &amp; 15th Ave, Apt 5 San Francisco, CA, 230903</p>
                        </div>
                    </div>
                    <div class="four columns omega">
                        <div class="content_div">
                            <span class="pix_text"><span class="editContent footer3_title">Info</span></span>
                            <p class="editContent ">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt Lorem ipsum dolor sit amet consectetur adipiscing elit sed.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pixfort_party_15" id="section_real_estate_9">
            <div class="foot_st pix_builder_bg">
                <div class="container ">
                    <div class="seven columns alpha ">
                        <span class="editContent">
                            <span class="pix_text">
                                <span class="rights_st">
                                    All rights reserved Copyright © 2014 FLATPACK by <span class="pixfort_st">PixFort</span>
                                </span>
                            </span>
                        </span>
                    </div>
                    <div class="nine columns omega ">
                        <div class="socbuttons">
                            <div class="soc_icons pix_builder_bg">
                                <ul class="bottom-icons">
                                    <li><a class="pi pixicon-facebook2 normal_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-twitter2 normal_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-instagram normal_gray" href="#fakelink"></a></li>
                                </ul>
                            </div>
                            <div class="likes_st">
                                <span class="editContent"><span class="pix_text">Your likes &amp; share makes us happy!</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <!--  </div> -->

    