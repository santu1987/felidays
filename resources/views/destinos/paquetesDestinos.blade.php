@extends('templates.main')

@section('title')
	Felidays - Guíamos tus emociones
@endsection

@section('cuerpo_css')
	<link rel="stylesheet" href="{{asset('stylesheets/felidays2.css')}}">
@endsection

@section('cuerpo_js')
	<script type="text/javascript" src="{{asset('assets/js/funcionesModulos/paquetesPaisCiudad.js')}}"></script>
@endsection

@section('contenido')
	@include('destinos.destinos',$ciudad)
@endsection