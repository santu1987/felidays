<div class="col-lg-12 padding0 m-top-85">
    <!--TITULO SEPARADOR -->
    <div class="padding-ciudad pix_builder_bg dark event_bg" id="section_normal_4_1" style="background: linear-gradient(rgba(0,0,0,0.25),rgba(0,0,0,0.25)),url(/images/parallax/destinos/{{$ciudad[0]['imagen']}}) #FF3300 fixed no-repeat">
        <div class="container">
            <div class="sixteen columns left_text">
                <p class="big_title editContent"><strong>{{$ciudad[0]["nombre"]}}</strong></p>
                <div class="row padding-desc-ciudad">
                    <div class="col-lg-4 center_text">
                        <div class="row">
                            <div class="col-lg-12 normal_text max_600 editContent texto_blanco ">
                                Opiniones
                            </div> 
                            <div class="col-lg-12 normal_text max_600 editContent texto_blanco fuente-desc-ciudad">
                                <strong>100</strong>
                            </div>
                        </div>    
                    </div>
                    
                    <div class="col-lg-4 center_text">
                        <div class="row">
                            <div class="col-lg-12 normal_text max_600 editContent texto_blanco">
                                Excursiones
                            </div>
                            <div class="col-lg-12 normal_text max_600 editContent texto_blanco fuente-desc-ciudad">
                                <strong>10</strong>
                            </div>
                        </div>    
                    </div>
                    
                    <div class="col-lg-4 center_text">
                        <div class="normal_text max_600 editContent texto_blanco">
                            <strong>Valoración</strong>
                        </div>
                        <div class="col-lg-12 padding0">
                            <span class="text-left c-blanco-valoracion">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star-o"></span>
                            </span>
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
    </div>
    <!--FIN TITULO SEPARADOR-->
</div>
<div class="fondo_gris">
    <div class="container ">
        <div class="col-12">
            <div class="row " >
                <div class="col-lg-6 div-filtros">
                    <select class="form-control">
                        <option>Pais</option>
                    </select>
                </div>
                <div class="col-lg-6 div-filtros">
                    <select class="form-control">
                        <option>Ciudad</option>
                    </select>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
        <div class="col-sm-4 padding-left0">
            <div id="filtros" class="card-padre-destino">
                <div class="card">
                  <div class="card-header">
                    Categorías
                  </div>
                  <div class="card-body">
                    <ul class="m-checklist">
                        <li>
                            <div class="form-check">
                                <label>
                                    <input type="checkbox" name="check"> 
                                    <span class="label-text">Visitas guiadas</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div class="form-check">
                                <label>
                                    <input type="checkbox" name="check"> 
                                    <span class="label-text">Museos y monumentos</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div class="form-check">
                                <label>
                                    <input type="checkbox" name="check"> 
                                    <span class="label-text">Excursiones de un día</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div class="form-check">
                                <label>
                                    <input type="checkbox" name="check"> 
                                    <span class="label-text">Excursiones de varios días</span>
                                </label>
                            </div>
                        </li>
                    </ul>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    Duración
                  </div>
                  <div class="card-body">
                    <div class="slidecontainer">
                      <input type="range" min="1" max="5" value="50" class="slider" id="myRange1">
                      <p><span id="demo1"></span>Hrs</p>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header">
                    Precio
                  </div>
                  <div class="card-body">
                    <div class="slidecontainer">
                      <input type="range" min="1" max="100" value="50" class="slider" id="myRange2">
                      <p><span id="demo2"></span>$</p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 padding-right0">
            <div id="ciudad" class="">
                <div class="card card-destino">
                        <div class="row row-destino">
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div>
                                        <h3 class="text-left c-naranja-oscuro">VISITA AL COLISEO</h3>
                                        <div class="col-md-12 text-left p-left-0 texto-opiniones">
                                            <div class="col-lg-6 padding0">
                                                <span class="text-left c-naranja-oscuro">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star-o"></span>
                                                </span>
                                            </div>
                                            <div class="col-lg-6 padding0">
                                                <span class="texto-opiniones3">
                                                    430 opiniones
                                                </span>
                                            </div>

                                        </div>
                                        <div style="clear: both"></div>
                                    </div>        
                                    <p class="card-text" style="margin-top:20px;">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    
                                    <div style="clear: both"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">    
                                <img class="card-img-top img-responsive" src="/images/cards/1.jpg" alt="Card image cap">    
                            </div>
                            <div class="col-lg-6 texto-opiniones3 footer-destino">
                                <div class="col-lg-6"><span class="fa fa-clock-o" ></span>  5 hrs</div>
                                <div class="col-lg-6"><span class="fa fa-comment-o" ></span> Esp</div>
                            </div>
                            <div class="col-lg-6 texto-opiniones3 footer-precio">
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6"><span class="fa fa-usd"></span> 50</div>
                            </div> 
                        </div>    
                </div>
                <div class="card card-destino">
                        <div class="row row-destino">
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div>
                                        <h3 class="text-left c-naranja-oscuro">VISITA AL COLISEO</h3>
                                        <div class="col-md-12 text-left p-left-0 texto-opiniones">
                                            <div class="col-lg-6 padding0">
                                                <span class="text-left c-naranja-oscuro">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star-o"></span>
                                                </span>
                                            </div>
                                            <div class="col-lg-6 padding0">
                                                <span class="texto-opiniones3">
                                                    430 opiniones
                                                </span>
                                            </div>

                                        </div>
                                        <div style="clear: both"></div>
                                    </div>        
                                    <p class="card-text" style="margin-top:20px;">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    
                                    <div style="clear: both"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">    
                                <img class="card-img-top img-responsive" src="/images/cards/1.jpg" alt="Card image cap">    
                            </div>
                            <div class="col-lg-6 texto-opiniones3 footer-destino">
                                <div class="col-lg-6"><span class="fa fa-clock-o" ></span>  5 hrs</div>
                                <div class="col-lg-6"><span class="fa fa-comment-o" ></span> Esp</div>
                            </div>
                            <div class="col-lg-6 texto-opiniones3 footer-precio">
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6"><span class="fa fa-usd"></span> 50</div>
                            </div> 
                        </div>    
                </div>
                <div class="card card-destino">
                        <div class="row row-destino">
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div>
                                        <h3 class="text-left c-naranja-oscuro">VISITA AL COLISEO</h3>
                                        <div class="col-md-12 text-left p-left-0 texto-opiniones">
                                            <div class="col-lg-6 padding0">
                                                <span class="text-left c-naranja-oscuro">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star-o"></span>
                                                </span>
                                            </div>
                                            <div class="col-lg-6 padding0">
                                                <span class="texto-opiniones3">
                                                    430 opiniones
                                                </span>
                                            </div>

                                        </div>
                                        <div style="clear: both"></div>
                                    </div>        
                                    <p class="card-text" style="margin-top:20px;">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    
                                    <div style="clear: both"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">    
                                <img class="card-img-top img-responsive" src="/images/cards/1.jpg" alt="Card image cap">    
                            </div>
                            <div class="col-lg-6 texto-opiniones3 footer-destino">
                                <div class="col-lg-6"><span class="fa fa-clock-o" ></span>  5 hrs</div>
                                <div class="col-lg-6"><span class="fa fa-comment-o" ></span> Esp</div>
                            </div>
                            <div class="col-lg-6 texto-opiniones3 footer-precio">
                                <div class="col-lg-6"></div>
                                <div class="col-lg-6"><span class="fa fa-usd"></span> 50</div>
                            </div> 
                        </div>    
                </div>
            </div>
        </div>
        <div style="clear:both"></div>
    </div>
</div>    