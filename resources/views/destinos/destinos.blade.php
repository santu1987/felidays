<div class="col-lg-12 padding0 m-top-85">
    <!--TITULO SEPARADOR -->
    <div class="padding-ciudad pix_builder_bg dark event_bg" id="section_normal_4_1" style="background: linear-gradient(rgba(0,0,0,0.25),rgba(0,0,0,0.25)),url(/images/parallax/destinos/{{$ciudad[0]['imagen']}}) #FF3300 fixed no-repeat">
        <div class="container">
            <div class="sixteen columns center_text">
                <p class="big_title editContent"><strong>TEXTO DE PRUEBA</strong></p>
                <div class="row padding-desc-ciudad">
                    <div class="col-lg-4 center_text">
                        <div class="row">
                            <div class="col-lg-12 normal_text max_600 editContent texto_blanco ">
                            </div> 
                            <div class="col-lg-12 normal_text max_600 editContent texto_blanco fuente-desc-ciudad">
                            </div>
                        </div>    
                    </div>
                    
                    <div class="col-lg-4 center_text">
                        <div class="row">
                            
                        </div>    
                    </div>
                    
                    <div class="col-lg-4 center_text">
                        <div class="normal_text max_600 editContent texto_blanco">
                            
                        </div>
                        <div class="col-lg-12 padding0">
                           
                        </div>
                    </div>
                    
                </div>
            </div>
       </div>
    </div>
    <!--FIN TITULO SEPARADOR-->
</div>
<div class="fondo_gris">
    <div class="container ">
        <div class="col-12">
            <div class="row " >
                <div class="col-lg-6 div-filtros">
                    <select class="form-control">
                        <option>Pais</option>
                    </select>
                </div>
                <div class="col-lg-6 div-filtros">
                    <select class="form-control">
                        <option>Ciudad</option>
                    </select>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
        <div class="cuerpo-img-destino-nivel-i">
           <div class="col-lg-4 contenedor-img-destino">
               <div class="project">
                   <span  class="mask mask-destino">
                        <span class="info texto-mask-destino">
                            <h3>Ciudad xxx</h3>
                        </span>  

                        <span class="texto-opiniones2 texto-mask-destino-inf">
                            <h4>
                                <span class="text-left c-blanco-valoracion">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                </span>
                            </h4>
                        </span>
                    </span>
                    <span>
                           <img src="/images/cards/1.jpg" class="img-responsive img-destino-nivel-i">
                           <div class="texto-encima-destino col-lg-12">Destino xxx</div>
                    </span>
                </div>    
           </div>
           <div class="col-lg-4 contenedor-img-destino">
               <div class="project">
                   <span  class="mask mask-destino">
                        <span class="info texto-mask-destino">
                            <h3>Ciudad xxx</h3>
                        </span>  

                        <span class="texto-opiniones2 texto-mask-destino-inf">
                            <h4>
                                <span class="text-left c-blanco-valoracion">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                </span>
                            </h4>
                        </span>
                    </span>
                    <span>
                           <img src="/images/cards/2.jpg" class="img-responsive img-destino-nivel-i">
                           <div class="texto-encima-destino col-lg-12">Destino xxx</div>
                    </span>
                </div>    
           </div>
           <div class="col-lg-4 contenedor-img-destino">
               <div class="project">
                   <span  class="mask mask-destino">
                        <span class="info texto-mask-destino">
                            <h3>Ciudad xxx</h3>
                        </span>  

                        <span class="texto-opiniones2 texto-mask-destino-inf">
                            <h4>
                                <span class="text-left c-blanco-valoracion">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                </span>
                            </h4>
                        </span>
                    </span>
                    <span>
                           <img src="/images/cards/3.jpg" class="img-responsive img-destino-nivel-i">
                           <div class="texto-encima-destino col-lg-12">Destino xxx</div>
                    </span>
                </div>    
           </div>
           <div class="col-lg-4 contenedor-img-destino">
               <div class="project">
                   <span  class="mask mask-destino">
                        <span class="info texto-mask-destino">
                            <h3>Ciudad xxx</h3>
                        </span>  

                        <span class="texto-opiniones2 texto-mask-destino-inf">
                            <h4>
                                <span class="text-left c-blanco-valoracion">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                </span>
                            </h4>
                        </span>
                    </span>
                    <span>
                           <img src="/images/cards/4.jpg" class="img-responsive img-destino-nivel-i">
                           <div class="texto-encima-destino col-lg-12">Destino xxx</div>
                    </span>
                </div>    
           </div>
           <div class="col-lg-4 contenedor-img-destino">
               <div class="project">
                   <span  class="mask mask-destino">
                        <span class="info texto-mask-destino">
                            <h3>Ciudad xxx</h3>
                        </span>  

                        <span class="texto-opiniones2 texto-mask-destino-inf">
                            <h4>
                                <span class="text-left c-blanco-valoracion">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                </span>
                            </h4>
                        </span>
                    </span>
                    <span>
                           <img src="/images/cards/5.jpg" class="img-responsive img-destino-nivel-i">
                           <div class="texto-encima-destino col-lg-12">Destino xxx</div>
                    </span>
                </div>    
           </div>
           <div class="col-lg-4 contenedor-img-destino">
               <div class="project">
                   <span  class="mask mask-destino">
                        <span class="info texto-mask-destino">
                            <h3>Ciudad xxx</h3>
                        </span>  

                        <span class="texto-opiniones2 texto-mask-destino-inf">
                            <h4>
                                <span class="text-left c-blanco-valoracion">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star-o"></span>
                                </span>
                            </h4>
                        </span>
                    </span>
                    <span>
                           <img src="/images/cards/6.jpg" class="img-responsive img-destino-nivel-i">
                           <div class="texto-encima-destino col-lg-12">Destino xxx</div>
                    </span>
                </div>    
           </div>
           <div style="clear: both;"></div>
        </div>
        <div class="text-center big_padding2 fondo_gris" style="padding-bottom: 50px;">
            <div id="btn_ver_mas" class="pix_button btn_normal small_wide_button dark slow_fade light_blue_bg editContent small_bold" style="cursor:pointer" ><strong>Ver mas destinos</strong></div>
        </div>
        <div style="clear:both"></div>
    </div>
</div>    