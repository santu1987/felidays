create database felidays;

use felidays;

create table Idiomas(
	idIdioma int not null auto_increment primary key,
    nombre varchar(20) not null,
    descripcion text,
    estatus int not null,
    bandera varchar(100) not null
);

create table Sliders(
	idSlider int not null auto_increment primary key,
    idIdioma int not null,
    titulo varchar(100) not null,
    subtitulo varchar(100),
    imagen varchar(150) not null,
    estatus int not null,
    seccion varchar(20) not null,
    boton text,
    tipo varchar(20),
    orientacion varchar(50),
    bcolor varchar(20),
    descripcion text,
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade
);

create table Empresas(
	idEmpresa int not null auto_increment primary key,
    idIdioma int not null,
    estatus int not null,
    qSomos text not null,
    mision text,
    vision text,
    historia text,
    principio text,
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade
);

create table Guias(
	idGuia int not null auto_increment primary key,
    idIdioma int not null,
    nombre varchar(100) not null,
    estatus int not null,
    imagen varchar(100),
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade
);

create table Evaluaciones(
	idEvaluacion int not null auto_increment primary key,
    idGuia int not null,
    valor int not null,
    foreign key (idGuia) references Guias(idGuia) on delete cascade on update cascade
);

create table SeccionesParallax(
	idSeccionP int not null auto_increment primary key,
    idIdioma int not null,
    nombre varchar(100) not null,
    titulo varchar(100) not null,
    subtitulo varchar(100),
    seccion varchar(20) not null,
    imagen varchar(100) not null,
    estatus int not null,
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade
);

create table Paises(
	idPais int not null auto_increment primary key,
    idIdioma int not null,
    nombre varchar(50) not null,
    bandera varchar(100),
    estatus int not null,
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade
);

create table Ciudades(
	idCiudad int not null auto_increment primary key,
    idPais int not null,
    nombre varchar(100) not null,
    estatus int not null,
    foreign key (idPais) references Paises(idPais) on delete cascade on update cascade
);

create table footer(
	idFooter int not null auto_increment primary key,
    idIdioma int not null,
    info text not null,
    columnas text not null,
    formasPagos text,
    redesSociales text not null,
    copy varchar(100) not null,
    estatus int not null,
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade
);

create table Contactos(
	idContacto int not null auto_increment primary key,
    idIdioma int null,
    email varchar(100) not null,
    telefono varchar(20) not null,
    direccion text,
    latitud varchar(20),
    longitud varchar(20),
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade
);

create table Formularios(
	idFormulario int not null auto_increment primary key,
    idIdioma int not null,
    idPais int not null,
    nombre varchar(100) not null,
    email varchar(100) not null,
    telefono varchar(20) not null,
    tipo varchar(20) not null,
    documentoIdentidad varchar(20),
    direccion text,
    ciudad varchar(50) not null,
    mensaje text not null,
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade,
    foreign key (idPais) references Paises(idPais) on delete cascade on update cascade
);

create table Usuarios(
	idUsuario int not null auto_increment primary key,
    nombre varchar(100) not null,
    usuario varchar(20) not null,
    clave varchar(100) not null,
    tipo int not null,
    metodoRegistro varchar(30) not null
);

create table Paquetes(
	idPaquete int not null auto_increment primary key,
    idPais int not null,
    idCiudad int not null,
    idIdioma int not null,
    titulo varchar(100) not null,
    idioma varchar(20) not null,
    etiqueta varchar(50),
    precio text not null,
    descripcion text not null,
    itinerario text,
    puntoEcuentro text,
    descripcionDuracion text,
    adicional text,
    preguntasFrecuentes text,
    fecha text not null,
    seccionVIP text,
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade,
    foreign key (idPais) references Paises(idPais) on delete cascade on update cascade,
    foreign key (idCiudad) references Ciudades(idCiudad) on delete cascade on update cascade
);

create table PaquetesGuias(
	idPG int not null auto_increment primary key,
    idGuia int not null,
    idPaquete int not null,
    foreign key (idGuia) references Guias(idGuia) on delete cascade on update cascade,
    foreign key (idPaquete) references Paquetes(idPaquete) on delete cascade on update cascade
);

create table Encuestas(
	idEncuesta int not null auto_increment primary key,
    idUsuario int not null,
    estatus int not null,
    descripcion text,
    tipo varchar(20) not null,
    idtipo int not null,
    valor int not null,
    nacionalidad varchar(30),
    foreign key (idUsuario) references Usuarios(idUsuario) on delete cascade on update cascade
);

create table Carritos(
	idCarrito int not null auto_increment primary key,
    idPaquete int not null,
    idIdioma int not null,
    detalle text not null,
    fecha date not null,
    hora varchar(10) not null,
    tipoPago varchar(20),
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade,
    foreign key (idPaquete) references Paquetes(idPaquete) on delete cascade on update cascade
);

create table rutas(
	id int not null primary key auto_increment,
    idIdioma int not null,
    seccion varchar(20) not null,
    ruta text not null,
    foreign key (idIdioma) references Idiomas(idIdioma) on delete cascade on update cascade
)