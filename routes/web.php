<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
//Inicio...
Route::get('/{Idioma}', 'WEB\InicioController@index');

//***********************RUTAS APIs **********************************

//EMPRESA
Route::get('API/consultarEmpresas/{idIdioma}', 'API\QuienesSomosController@getEmpresas');

//CONTACTOS
Route::get('API/consultarContacto/{idIdioma}', 'API\ContactosController@getContactos');
Route::post('API/formularioRegistro', 'API\ContactosController@postFormulario');

//EQUIPO
Route::get('API/consultarEquipo/{idIdioma}', 'API\EquipoController@getEquipo');

//REUTILIZABLES
Route::get('API/consultarPaises/{idIdioma}', 'API\ReutilizableController@getPaises');
Route::get('API/consultarIdiomas', 'API\ReutilizableController@getIdiomas');
Route::get('API/consultarFooterIdioma/{idIdioma}', 'API\ReutilizableController@getFooter');
Route::get('API/consultarCiudadesAll', 'API\ReutilizableController@getCiudades');
Route::get('API/consultarCiudadesPais/{idPais}', 'API\ReutilizableController@getCiudadesPais');
Route::get('API/consultarParallax/{idIdioma}/{seccion}', 'API\ReutilizableController@getParallax');
Route::get('API/consultarRutas/{idIdioma}', 'API\ReutilizableController@getRutas');
Route::get('API/consultarCiudad/{idCiudad}','API\ReutilizableController@getCiudadById');

/*
 *	Home
 */

Route::get('API/consultarSlider/{idIdioma}', 'API\HomeController@getSlider');
Route::get('API/consultarParallax/{idIdioma}', 'API\HomeController@getParallax');
Route::get('API/consultarFooter/{idIdioma}', 'API\HomeController@getFooter');

/*
 *	Guias
 */
Route::get('API/consultarGuiasDetalle/{idioma}/{id}', 'API\GuiasController@consultarGuiasDetalle');
Route::get('API/consultarGuias/{idIdioma}/{limit}/{offset}', 'API\GuiasController@getGuias');
Route::get('API/consultarParallaxGuias/{idIdioma}', 'API\GuiasController@getParallax');
/*
 *	Paquetes
 */
Route::get('API/consultarPaquetes/{idIdioma}', 'API\PaquetesController@getPaquetes');
Route::get('API/consultarPaquetesByPais/{idIdioma}/{idPais}', 'API\PaquetesController@getPaquetesByPais');
Route::get('API/consultarPaquetesByCiudad/{idIdioma}/{idCiudad}', 'API\PaquetesController@getPaquetesByCiudad');

//*********************** FIN RUTAS APIs **********************************

//***********************RUTAS DE LA WEB **********************************
//ESPAÑOL
Route::get('quienesSomos/{Idioma}', 'WEB\QuienesSomosController@getIndex');
Route::get('destinos/{Idioma}', 'WEB\DestinosController@getIndex');
//--Guias
Route::get('guias/{Idioma}', 'WEB\GuiasController@getIndex');
Route::post('verMasGuias', 'WEB\GuiasController@verMas');
Route::get('guias/{Idioma}/{id}', 'WEB\GuiasController@verGuiasDetalle');
//--Destinos
Route::get('destinos/{idCiudad}/{idPais}/{Idioma}','WEB\DestinosController@getDestinoCiudadPais');
Route::get('destinos/{Idioma}','WEB\DestinosController@getDestino');

//--
Route::get('cotizador/{Idioma}', 'WEB\CotizadorController@getIndex');
Route::get('contacto/{Idioma}', 'WEB\ContactosController@getIndex');

//INGLES
Route::get('aboutUs/{Idioma}', 'WEB\QuienesSomosController@getIndex');
Route::get('trips/{Idioma}', 'WEB\DestinosController@getIndex');
Route::get('guides/{Idioma}', 'WEB\GuiasController@getIndex');
Route::get('quoting/{Idioma}', 'WEB\CotizadorController@getIndex');
Route::get('contact/{Idioma}', 'WEB\ContactosController@getIndex');
